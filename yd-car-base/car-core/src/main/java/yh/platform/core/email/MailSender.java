package yh.platform.core.email;


import yh.platform.core.email.modular.model.SendMailParam;

/**
 * 邮件收发统一接口
 */
public interface MailSender {

    /**
     * 发送普通邮件
     */
    void sendMail(SendMailParam sendMailParam);

    /**
     * 发送html的邮件
     */
    void sendMailHtml(SendMailParam sendMailParam);

}
