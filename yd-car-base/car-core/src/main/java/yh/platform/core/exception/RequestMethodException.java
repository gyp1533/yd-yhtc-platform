package yh.platform.core.exception;

import lombok.Getter;
import yh.platform.core.exception.enums.abs.AbstractBaseExceptionEnum;

/**
 * 请求方法异常
 *
 * @author gyp
 */
@Getter
public class RequestMethodException extends RuntimeException {

    private final Integer code;

    private final String errorMessage;

    public RequestMethodException(AbstractBaseExceptionEnum exception) {
        super(exception.getMessage());
        this.code = exception.getCode();
        this.errorMessage = exception.getMessage();
    }
}
