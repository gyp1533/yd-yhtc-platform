package yh.platform.core.tenant.exception;

import yh.platform.core.exception.ServiceException;
import yh.platform.core.exception.enums.abs.AbstractBaseExceptionEnum;

/**
 * 多租户的异常
 */
public class TenantException extends ServiceException {

    public TenantException(AbstractBaseExceptionEnum exception) {
        super(exception);
    }

}
