package yh.platform.modular.appuser.service;

import com.baomidou.mybatisplus.extension.service.IService;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.modular.appuser.entity.AppUser;
import yh.platform.modular.appuser.param.AppUserParam;
import java.util.List;

/**
 * 用户表service接口
 *
 * @author sht
 * @date 2023-07-26 20:49:35
 */
public interface AppUserService extends IService<AppUser> {

    /**
     * 查询用户表
     *
     * @author sht
     * @date 2023-07-26 20:49:35
     */
    PageResult<AppUser> page(AppUserParam appUserParam);

    /**
     * 用户表列表
     *
     * @author sht
     * @date 2023-07-26 20:49:35
     */
    List<AppUser> list(AppUserParam appUserParam);

    /**
     * 添加用户表
     *
     * @author sht
     * @date 2023-07-26 20:49:35
     */
    void add(AppUserParam appUserParam);

    /**
     * 删除用户表
     *
     * @author sht
     * @date 2023-07-26 20:49:35
     */
    void delete(List<AppUserParam> appUserParamList);

    /**
     * 编辑用户表
     *
     * @author sht
     * @date 2023-07-26 20:49:35
     */
    void edit(AppUserParam appUserParam);

    /**
     * 查看用户表
     *
     * @author sht
     * @date 2023-07-26 20:49:35
     */
     AppUser detail(AppUserParam appUserParam);

    /**
     * 导出用户表
     *
     * @author sht
     * @date 2023-07-26 20:49:35
     */
     void export(AppUserParam appUserParam);

}
