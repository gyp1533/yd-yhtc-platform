package yh.platform.modular.appuser.controller;

import yh.platform.core.annotion.BusinessLog;
import yh.platform.core.annotion.Permission;
import yh.platform.core.enums.LogAnnotionOpTypeEnum;
import yh.platform.core.pojo.response.ResponseData;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.core.pojo.response.SuccessResponseData;
import yh.platform.modular.appuser.entity.AppUser;
import yh.platform.modular.appuser.param.AppUserParam;
import yh.platform.modular.appuser.service.AppUserService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 用户表控制器
 *
 * @author sht
 * @date 2023-07-26 20:49:35
 */
@RestController
public class AppUserController {

    @Resource
    private AppUserService appUserService;

    /**
     * 查询用户表
     *
     * @author sht
     * @date 2023-07-26 20:49:35
     */
    @GetMapping("/appUser/page")
    public ResponseData page(AppUserParam appUserParam) {
        return new SuccessResponseData(appUserService.page(appUserParam));
    }

    /**
     * 添加用户表
     *
     * @author sht
     * @date 2023-07-26 20:49:35
     */
    @PostMapping("/appUser/add")
    public void add(@RequestBody @Validated(AppUserParam.add.class) AppUserParam appUserParam) {
            appUserService.add(appUserParam);
    }

    /**
     * 删除用户表，可批量删除
     *
     * @author sht
     * @date 2023-07-26 20:49:35
     */
    @PostMapping("/appUser/delete")
    public void delete(@RequestBody @Validated(AppUserParam.delete.class) List<AppUserParam> appUserParamList) {
            appUserService.delete(appUserParamList);
    }

    /**
     * 编辑用户表
     *
     * @author sht
     * @date 2023-07-26 20:49:35
     */
    @PostMapping("/appUser/edit")
    public void edit(@RequestBody @Validated(AppUserParam.edit.class) AppUserParam appUserParam) {
            appUserService.edit(appUserParam);
    }

    /**
     * 查看用户表
     *
     * @author sht
     * @date 2023-07-26 20:49:35
     */
    @PostMapping("/appUser/detail")
    public AppUser detail(@Validated(AppUserParam.detail.class) AppUserParam appUserParam) {

        return appUserService.detail(appUserParam);
    }

    /**
     * 用户表列表
     *
     * @author sht
     * @date 2023-07-26 20:49:35
     */
    @PostMapping("/appUser/list")
    public List<AppUser> list(AppUserParam appUserParam) {
        return appUserService.list(appUserParam);
    }

    /**
     * 导出系统用户
     *
     * @author sht
     * @date 2023-07-26 20:49:35
     */
    @PostMapping("/appUser/export")
    public void export(AppUserParam appUserParam) {
        appUserService.export(appUserParam);
    }

}
