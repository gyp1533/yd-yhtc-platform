package yh.platform.modular.appuser.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.afterturn.easypoi.excel.annotation.Excel;
import yh.platform.core.pojo.base.entity.BaseEntity;

import java.util.Date;

/**
 * 用户表
 *
 * @author sht
 * @date 2023-07-26 20:49:35
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("app_user")
public class AppUser extends BaseEntity {

    /**
     * 
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 用户名称
     */
    @Excel(name = "用户名称")
    private String userName;

    /**
     * 用户手机号
     */
    @Excel(name = "用户名称")
    private String phoneNumber;

    /**
     * 登录密码
     */
    @Excel(name = "登录密码")
    private String password;

    /**
     * 密码盐
     */
    @Excel(name = "密码盐")
    private String passwordSalt;

    /**
     * 最近登录IP
     */
    @Excel(name = "最近登录IP")
    private String lastLoginIp;

    /**
     * 最近登录时间
     */
    @Excel(name = "最近登录时间", databaseFormat = "yyyy-MM-dd HH:mm:ss", format = "yyyy-MM-dd", width = 20)
    private Date lastLoginTime;

    /**
     * 0已锁定 1正常用户
     */
    @Excel(name = "0已锁定 1正常用户")
    private Integer status;

    /**
     * 1:中文 2:英语 3:俄语
     */
    @Excel(name = "1:中文 2:英语 3:俄语")
    private Integer language;

    /**
     * 0未删除 1已删除
     */
    @Excel(name = "0未删除 1已删除")
    private Integer deleteFlag;

    /**
     * 当前积分
     */
    @Excel(name = "当前积分")
    private Integer integral;

    /**
     * 
     */
    @Excel(name = "备注信息")
    private String remark;

}
