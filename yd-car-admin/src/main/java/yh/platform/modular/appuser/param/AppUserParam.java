package yh.platform.modular.appuser.param;

import yh.platform.core.pojo.base.param.BaseParam;
import lombok.Data;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

/**
* 用户表参数类
 *
 * @author sht
 * @date 2023-07-26 20:49:35
*/
@Data
public class AppUserParam extends BaseParam {

    /**
     * 
     */
    @NotNull(message = "不能为空，请检查id参数", groups = {edit.class, delete.class, detail.class})
    private String id;

    /**
     * 
     */
    @NotBlank(message = "不能为空，请检查username参数", groups = {add.class, edit.class})
    private String username;

    /**
     * 登录密码
     */
    @NotBlank(message = "登录密码不能为空，请检查password参数", groups = {add.class, edit.class})
    private String password;

    /**
     * 密码盐
     */
    @NotBlank(message = "密码盐不能为空，请检查passwordSalt参数", groups = {add.class, edit.class})
    private String passwordSalt;

    /**
     * 最近登录IP
     */
    @NotBlank(message = "最近登录IP不能为空，请检查lastLoginIp参数", groups = {add.class, edit.class})
    private String lastLoginIp;

    /**
     * 最近登录时间
     */
    @NotNull(message = "最近登录时间不能为空，请检查lastLoginTime参数", groups = {add.class, edit.class})
    private String lastLoginTime;

    /**
     * 0已锁定 1正常用户
     */
    @NotNull(message = "0已锁定 1正常用户不能为空，请检查status参数", groups = {add.class, edit.class})
    private Integer status;

    /**
     * 1:中文 2:英语 3:俄语
     */
    @NotNull(message = "1:中文 2:英语 3:俄语不能为空，请检查language参数", groups = {add.class, edit.class})
    private Integer language;

    /**
     * 0未删除 1已删除
     */
    @NotNull(message = "0未删除 1已删除不能为空，请检查deleteFlag参数", groups = {add.class, edit.class})
    private Integer deleteFlag;

    /**
     * 
     */
    @NotNull(message = "不能为空，请检查createBy参数", groups = {add.class, edit.class})
    private Long createBy;

    /**
     * 
     */
    @NotNull(message = "不能为空，请检查updateBy参数", groups = {add.class, edit.class})
    private Long updateBy;

    /**
     * 
     */
    @NotBlank(message = "不能为空，请检查remark参数", groups = {add.class, edit.class})
    private String remark;

}
