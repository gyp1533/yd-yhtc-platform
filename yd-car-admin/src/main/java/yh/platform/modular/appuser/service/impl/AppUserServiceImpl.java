package yh.platform.modular.appuser.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import yh.platform.core.exception.ServiceException;
import yh.platform.core.factory.PageFactory;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.core.util.PoiUtil;
import yh.platform.modular.appuser.entity.AppUser;
import yh.platform.modular.appuser.enums.AppUserExceptionEnum;
import yh.platform.modular.appuser.mapper.AppUserMapper;
import yh.platform.modular.appuser.param.AppUserParam;
import yh.platform.modular.appuser.service.AppUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 用户表service接口实现类
 *
 * @author sht
 * @date 2023-07-26 20:49:35
 */
@Service
public class AppUserServiceImpl extends ServiceImpl<AppUserMapper, AppUser> implements AppUserService {

    @Override
    public PageResult<AppUser> page(AppUserParam appUserParam) {
        QueryWrapper<AppUser> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(appUserParam)) {

            // 根据 查询
            if (ObjectUtil.isNotEmpty(appUserParam.getUsername())) {
                queryWrapper.lambda().eq(AppUser::getUserName, appUserParam.getUsername());
            }
            // 根据登录密码 查询
            if (ObjectUtil.isNotEmpty(appUserParam.getPassword())) {
                queryWrapper.lambda().eq(AppUser::getPassword, appUserParam.getPassword());
            }
            // 根据密码盐 查询
            if (ObjectUtil.isNotEmpty(appUserParam.getPasswordSalt())) {
                queryWrapper.lambda().eq(AppUser::getPasswordSalt, appUserParam.getPasswordSalt());
            }
            // 根据最近登录IP 查询
            if (ObjectUtil.isNotEmpty(appUserParam.getLastLoginIp())) {
                queryWrapper.lambda().eq(AppUser::getLastLoginIp, appUserParam.getLastLoginIp());
            }
            // 根据最近登录时间 查询
            if (ObjectUtil.isNotEmpty(appUserParam.getLastLoginTime())) {
                queryWrapper.lambda().eq(AppUser::getLastLoginTime, appUserParam.getLastLoginTime());
            }
            // 根据0已锁定 1正常用户 查询
            if (ObjectUtil.isNotEmpty(appUserParam.getStatus())) {
                queryWrapper.lambda().eq(AppUser::getStatus, appUserParam.getStatus());
            }
            // 根据1:中文 2:英语 3:俄语 查询
            if (ObjectUtil.isNotEmpty(appUserParam.getLanguage())) {
                queryWrapper.lambda().eq(AppUser::getLanguage, appUserParam.getLanguage());
            }
            // 根据0未删除 1已删除 查询
            if (ObjectUtil.isNotEmpty(appUserParam.getDeleteFlag())) {
                queryWrapper.lambda().eq(AppUser::getDeleteFlag, appUserParam.getDeleteFlag());
            }
            // 根据 查询
            if (ObjectUtil.isNotEmpty(appUserParam.getRemark())) {
                queryWrapper.lambda().eq(AppUser::getRemark, appUserParam.getRemark());
            }
        }
        return new PageResult<>(this.page(PageFactory.defaultPage(), queryWrapper));
    }

    @Override
    public List<AppUser> list(AppUserParam appUserParam) {
        return this.list();
    }

    @Override
    public void add(AppUserParam appUserParam) {
        AppUser appUser = new AppUser();
        BeanUtil.copyProperties(appUserParam, appUser);
        this.save(appUser);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<AppUserParam> appUserParamList) {
        appUserParamList.forEach(appUserParam -> {
            this.removeById(appUserParam.getId());
        });
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(AppUserParam appUserParam) {
        AppUser appUser = this.queryAppUser(appUserParam);
        BeanUtil.copyProperties(appUserParam, appUser);
        this.updateById(appUser);
    }

    @Override
    public AppUser detail(AppUserParam appUserParam) {
        return this.queryAppUser(appUserParam);
    }

    /**
     * 获取用户表
     *
     * @author sht
     * @date 2023-07-26 20:49:35
     */
    private AppUser queryAppUser(AppUserParam appUserParam) {
        AppUser appUser = this.getById(appUserParam.getId());
        if (ObjectUtil.isNull(appUser)) {
            throw new ServiceException(AppUserExceptionEnum.NOT_EXIST);
        }
        return appUser;
    }

    @Override
    public void export(AppUserParam appUserParam) {
        List<AppUser> list = this.list(appUserParam);
        PoiUtil.exportExcelWithStream("AppUser.xls", AppUser.class, list);
    }

}
