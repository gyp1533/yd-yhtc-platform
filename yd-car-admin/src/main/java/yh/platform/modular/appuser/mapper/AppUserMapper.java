package yh.platform.modular.appuser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import yh.platform.modular.appuser.entity.AppUser;

/**
 * 用户表
 *
 * @author gyp
 * @date 2023-07-28 08:49:24
 */
public interface AppUserMapper extends BaseMapper<AppUser> {
}
