package yh.platform.modular.carsubbrand.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.compress.utils.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import yh.platform.core.exception.ServiceException;
import yh.platform.core.factory.PageFactory;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.modular.carseries.entity.CarSeries;
import yh.platform.modular.carseries.param.CarSeriesParam;
import yh.platform.modular.carseries.service.CarSeriesService;
import yh.platform.modular.carsubbrand.entity.CarSubBrand;
import yh.platform.modular.carsubbrand.enums.CarSubBrandExceptionEnum;
import yh.platform.modular.carsubbrand.mapper.CarSubBrandMapper;
import yh.platform.modular.carsubbrand.param.CarSubBrandParam;
import yh.platform.modular.carsubbrand.service.CarSubBrandService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import yh.platform.modular.carsubbrand.vo.CarSubBrandVO;

import java.util.List;

/**
 * 车型库子品牌service接口实现类
 *
 * @author gyp
 * @date 2023-07-24 18:55:53
 */
@Service
public class CarSubBrandServiceImpl extends ServiceImpl<CarSubBrandMapper, CarSubBrand> implements CarSubBrandService {

    @Autowired
    private CarSeriesService carSeriesService;

    @Override
    public PageResult<CarSubBrand> page(CarSubBrandParam carSubBrandParam) {
        QueryWrapper<CarSubBrand> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(carSubBrandParam)) {

            // 根据 查询
            if (ObjectUtil.isNotEmpty(carSubBrandParam.getBrandId())) {
                queryWrapper.lambda().eq(CarSubBrand::getBrandId, carSubBrandParam.getBrandId());
            }
            // 根据 查询
            if (ObjectUtil.isNotEmpty(carSubBrandParam.getSubBrandName())) {
                queryWrapper.lambda().eq(CarSubBrand::getSubBrandName, carSubBrandParam.getSubBrandName());
            }
            // 根据 查询
            if (ObjectUtil.isNotEmpty(carSubBrandParam.getSubBrandCode())) {
                queryWrapper.lambda().eq(CarSubBrand::getSubBrandCode, carSubBrandParam.getSubBrandCode());
            }
            // 根据0,未删除1,已删除 查询
            if (ObjectUtil.isNotEmpty(carSubBrandParam.getDeleteFlag())) {
                queryWrapper.lambda().eq(CarSubBrand::getDeleteFlag, carSubBrandParam.getDeleteFlag());
            }
            // 根据 查询
            if (ObjectUtil.isNotEmpty(carSubBrandParam.getRemark())) {
                queryWrapper.lambda().eq(CarSubBrand::getRemark, carSubBrandParam.getRemark());
            }
        }
        return new PageResult<>(this.page(PageFactory.defaultPage(), queryWrapper));
    }

    @Override
    public List<CarSubBrandVO> list(CarSubBrandParam carSubBrandParam) {
        QueryWrapper<CarSubBrand> queryWrapper = new QueryWrapper<>();
        // 根据 查询
        if (ObjectUtil.isNotEmpty(carSubBrandParam.getId())) {
            queryWrapper.lambda().eq(CarSubBrand::getBrandId, carSubBrandParam.getId());
        }
        List<CarSubBrand> subBrands= this.list(queryWrapper);
        List<CarSubBrandVO> subBrandVOS = Lists.newArrayList();
        for (CarSubBrand carSubBrand:subBrands){
            CarSubBrandVO carSubBrandVO = new CarSubBrandVO();
            BeanUtil.copyProperties(carSubBrand, carSubBrandVO);
            CarSeriesParam carSeriesParam = new CarSeriesParam();
            carSeriesParam.setSubBrandId(carSubBrand.getId());
            List<CarSeries> carSeriesList = carSeriesService.list(carSeriesParam);
            carSubBrandVO.setCarSeriesList(carSeriesList);
            subBrandVOS.add(carSubBrandVO);
        }

        return subBrandVOS;
    }

    @Override
    public void add(CarSubBrandParam carSubBrandParam) {
        CarSubBrand carSubBrand = new CarSubBrand();
        BeanUtil.copyProperties(carSubBrandParam, carSubBrand);
        this.save(carSubBrand);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<CarSubBrandParam> carSubBrandParamList) {
        carSubBrandParamList.forEach(carSubBrandParam -> {
            this.removeById(carSubBrandParam.getId());
        });
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(CarSubBrandParam carSubBrandParam) {
        CarSubBrand carSubBrand = this.queryCarSubBrand(carSubBrandParam);
        BeanUtil.copyProperties(carSubBrandParam, carSubBrand);
        this.updateById(carSubBrand);
    }

    @Override
    public CarSubBrand detail(CarSubBrandParam carSubBrandParam) {
        return this.queryCarSubBrand(carSubBrandParam);
    }

    /**
     * 获取车型库子品牌
     *
     * @author gyp
     * @date 2023-07-24 18:55:53
     */
    private CarSubBrand queryCarSubBrand(CarSubBrandParam carSubBrandParam) {
        CarSubBrand carSubBrand = this.getById(carSubBrandParam.getId());
        if (ObjectUtil.isNull(carSubBrand)) {
            throw new ServiceException(CarSubBrandExceptionEnum.NOT_EXIST);
        }
        return carSubBrand;
    }


}
