package yh.platform.modular.carsubbrand.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.afterturn.easypoi.excel.annotation.Excel;
import yh.platform.core.pojo.base.entity.BaseEntity;

/**
 * 车型库子品牌
 *
 * @author gyp
 * @date 2023-07-24 18:55:53
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("car_sub_brand")
public class CarSubBrand extends BaseEntity {

    /**
     *
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     *
     */
    @Excel(name = "")
    private Long brandId;

    /**
     *
     */
    @Excel(name = "")
    private String subBrandName;

    /**
     *
     */
    @Excel(name = "")
    private String subBrandCode;

    /**
     * 0,未删除
            1,已删除
     */
    @Excel(name = "0,未删除1,已删除")
    private Integer deleteFlag;

    /**
     *
     */
    @Excel(name = "")
    private String remark;

}
