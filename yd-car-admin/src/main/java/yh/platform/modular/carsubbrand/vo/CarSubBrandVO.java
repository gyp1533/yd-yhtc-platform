package yh.platform.modular.carsubbrand.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import yh.platform.core.pojo.base.entity.BaseEntity;
import yh.platform.modular.carseries.entity.CarSeries;

import java.util.List;

/**
 * 车型库子品牌
 *
 * @author gyp
 * @date 2023-07-24 18:55:53
 */
@Data
public class CarSubBrandVO {

    private Long id;

    private Long brandId;

    private String subBrandName;

    private String subBrandCode;

    private List<CarSeries> carSeriesList;

}
