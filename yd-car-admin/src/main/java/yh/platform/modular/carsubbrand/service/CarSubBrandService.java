package yh.platform.modular.carsubbrand.service;

import com.baomidou.mybatisplus.extension.service.IService;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.modular.carsubbrand.entity.CarSubBrand;
import yh.platform.modular.carsubbrand.param.CarSubBrandParam;
import yh.platform.modular.carsubbrand.vo.CarSubBrandVO;

import java.util.List;

/**
 * 车型库子品牌service接口
 *
 * @author gyp
 * @date 2023-07-24 18:55:53
 */
public interface CarSubBrandService extends IService<CarSubBrand> {

    /**
     * 查询车型库子品牌
     *
     * @author gyp
     * @date 2023-07-24 18:55:53
     */
    PageResult<CarSubBrand> page(CarSubBrandParam carSubBrandParam);

    /**
     * 车型库子品牌列表
     *
     * @author gyp
     * @date 2023-07-24 18:55:53
     */
    List<CarSubBrandVO> list(CarSubBrandParam carSubBrandParam);

    /**
     * 添加车型库子品牌
     *
     * @author gyp
     * @date 2023-07-24 18:55:53
     */
    void add(CarSubBrandParam carSubBrandParam);

    /**
     * 删除车型库子品牌
     *
     * @author gyp
     * @date 2023-07-24 18:55:53
     */
    void delete(List<CarSubBrandParam> carSubBrandParamList);

    /**
     * 编辑车型库子品牌
     *
     * @author gyp
     * @date 2023-07-24 18:55:53
     */
    void edit(CarSubBrandParam carSubBrandParam);

    /**
     * 查看车型库子品牌
     *
     * @author gyp
     * @date 2023-07-24 18:55:53
     */
     CarSubBrand detail(CarSubBrandParam carSubBrandParam);

}
