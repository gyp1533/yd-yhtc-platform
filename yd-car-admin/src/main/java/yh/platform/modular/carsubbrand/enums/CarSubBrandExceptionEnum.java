package yh.platform.modular.carsubbrand.enums;

import yh.platform.core.annotion.ExpEnumType;
import yh.platform.core.exception.enums.abs.AbstractBaseExceptionEnum;
import yh.platform.core.factory.ExpEnumCodeFactory;
import yh.platform.sys.core.consts.SysExpEnumConstant;

/**
 * 车型库子品牌
 *
 * @author gyp
 * @date 2023-07-24 18:55:53
 */
@ExpEnumType(module = SysExpEnumConstant.YHTC_SYS_MODULE_EXP_CODE)
public enum CarSubBrandExceptionEnum implements AbstractBaseExceptionEnum {

    /**
     * 数据不存在
     */
    NOT_EXIST(1, "此数据不存在");

    private final Integer code;

    private final String message;
        CarSubBrandExceptionEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public Integer getCode() {
        return ExpEnumCodeFactory.getExpEnumCode(this.getClass(), code);
    }

    @Override
    public String getMessage() {
        return message;
    }

}
