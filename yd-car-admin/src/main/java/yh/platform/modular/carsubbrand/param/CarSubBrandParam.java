package yh.platform.modular.carsubbrand.param;

import yh.platform.core.pojo.base.param.BaseParam;
import lombok.Data;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

/**
* 车型库子品牌参数类
 *
 * @author gyp
 * @date 2023-07-24 18:55:53
*/
@Data
public class CarSubBrandParam extends BaseParam {

    /**
     *
     */
    @NotNull(message = "不能为空，请检查id参数", groups = {edit.class, delete.class, detail.class})
    private Long id;

    /**
     *
     */
    @NotNull(message = "不能为空，请检查brandId参数", groups = {add.class, edit.class})
    private Long brandId;

    /**
     *
     */
    @NotBlank(message = "不能为空，请检查subBrandName参数", groups = {add.class, edit.class})
    private String subBrandName;

    /**
     *
     */
    @NotBlank(message = "不能为空，请检查subBrandCode参数", groups = {add.class, edit.class})
    private String subBrandCode;

    /**
     * 0,未删除
            1,已删除
     */
    @NotNull(message = "0,未删除1,已删除不能为空，请检查deleteFlag参数", groups = {add.class, edit.class})
    private Integer deleteFlag;

    /**
     *
     */
    @NotNull(message = "不能为空，请检查createBy参数", groups = {add.class, edit.class})
    private Long createBy;

    /**
     *
     */
    @NotNull(message = "不能为空，请检查updateBy参数", groups = {add.class, edit.class})
    private Long updateBy;

    /**
     *
     */
    @NotBlank(message = "不能为空，请检查remark参数", groups = {add.class, edit.class})
    private String remark;

}
