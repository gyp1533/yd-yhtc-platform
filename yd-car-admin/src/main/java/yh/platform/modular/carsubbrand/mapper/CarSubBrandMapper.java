package yh.platform.modular.carsubbrand.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import yh.platform.modular.carsubbrand.entity.CarSubBrand;

/**
 * 车型库子品牌
 *
 * @author gyp
 * @date 2023-07-24 18:55:53
 */
public interface CarSubBrandMapper extends BaseMapper<CarSubBrand> {
}
