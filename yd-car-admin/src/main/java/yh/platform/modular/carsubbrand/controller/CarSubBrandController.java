package yh.platform.modular.carsubbrand.controller;

import yh.platform.core.annotion.BusinessLog;
import yh.platform.core.annotion.Permission;
import yh.platform.core.enums.LogAnnotionOpTypeEnum;
import yh.platform.core.pojo.response.ResponseData;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.core.pojo.response.SuccessResponseData;
import yh.platform.modular.carsubbrand.entity.CarSubBrand;
import yh.platform.modular.carsubbrand.param.CarSubBrandParam;
import yh.platform.modular.carsubbrand.service.CarSubBrandService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import yh.platform.modular.carsubbrand.vo.CarSubBrandVO;

import javax.annotation.Resource;
import java.util.List;

/**
 * 车型库子品牌控制器
 *
 * @author gyp
 * @date 2023-07-24 18:55:53
 */
@RestController
public class CarSubBrandController {

    @Resource
    private CarSubBrandService carSubBrandService;

//    /**
//     * 查询车型库子品牌
//     *
//     * @author gyp
//     * @date 2023-07-24 18:55:53
//     */
//    @PostMapping("/carSubBrand/page")
//    public ResponseData<PageResult<CarSubBrand>> page(CarSubBrandParam carSubBrandParam) {
//        return SuccessResponseData.success(carSubBrandService.page(carSubBrandParam));
//    }

//    /**
//     * 添加车型库子品牌
//     *
//     * @author gyp
//     * @date 2023-07-24 18:55:53
//     */
//    @PostMapping("/carSubBrand/add")
//    public ResponseData add(@RequestBody @Validated(CarSubBrandParam.add.class) CarSubBrandParam carSubBrandParam) {
//        carSubBrandService.add(carSubBrandParam);
//		return SuccessResponseData.success();
//    }

//    /**
//     * 删除车型库子品牌，可批量删除
//     *
//     * @author gyp
//     * @date 2023-07-24 18:55:53
//     */
//    @PostMapping("/carSubBrand/delete")
//    public ResponseData delete(@RequestBody @Validated(CarSubBrandParam.delete.class) List<CarSubBrandParam> carSubBrandParamList) {
//        carSubBrandService.delete(carSubBrandParamList);
//		return SuccessResponseData.success();
//    }

//    /**
//     * 编辑车型库子品牌
//     *
//     * @author gyp
//     * @date 2023-07-24 18:55:53
//     */
//    @PostMapping("/carSubBrand/edit")
//    public ResponseData edit(@RequestBody @Validated(CarSubBrandParam.edit.class) CarSubBrandParam carSubBrandParam) {
//        carSubBrandService.edit(carSubBrandParam);
//		return SuccessResponseData.success();
//    }

//    /**
//     * 查看车型库子品牌
//     *
//     * @author gyp
//     * @date 2023-07-24 18:55:53
//     */
//    @PostMapping("/carSubBrand/detail")
//    public ResponseData<CarSubBrand> detail(@RequestBody @Validated(CarSubBrandParam.detail.class) CarSubBrandParam carSubBrandParam) {
//
//        return SuccessResponseData.success(carSubBrandService.detail(carSubBrandParam));
//    }

    /**
     * 车型库子品牌列表
     *
     * @author gyp
     * @date 2023-07-24 18:55:53
     */
    @GetMapping("/car/carSubBrand/list")
    public ResponseData<List<CarSubBrandVO>> list(CarSubBrandParam carSubBrandParam) {
        return SuccessResponseData.success(carSubBrandService.list(carSubBrandParam));
    }

//    /**
//     * 导出系统用户
//     *
//     * @author gyp
//     * @date 2023-07-24 18:55:53
//     */
//    @PostMapping("/carSubBrand/export")
//    public void export(CarSubBrandParam carSubBrandParam) {
//        carSubBrandService.export(carSubBrandParam);
//    }

}
