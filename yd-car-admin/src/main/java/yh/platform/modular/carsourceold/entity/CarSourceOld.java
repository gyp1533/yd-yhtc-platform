package yh.platform.modular.carsourceold.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.afterturn.easypoi.excel.annotation.Excel;
import yh.platform.core.pojo.base.entity.BaseEntity;

import java.math.BigDecimal;

/**
 * 二手车车源
 *
 * @author gyp
 * @date 2023-08-03 22:48:06
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("car_source_old")
public class CarSourceOld extends BaseEntity {

    /**
     *
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 车源编号
     */
    @Excel(name = "车源编号")
    private String sourceNum;

    /**
     * 车型id
     */
    @Excel(name = "车型id")
    private Long modelId;

    /**
     * 指导价
     */
    @Excel(name = "指导价")
    private BigDecimal guidePrice;

    /**
     * 销售价格
     */
    @Excel(name = "销售价格")
    private BigDecimal salePrice;

    /**
     * 上牌时间
     */
    @Excel(name = "上牌时间")
    private String registrationTime;

    /**
     * 表显里程（万公里）
     */
    @Excel(name = "表显里程（万公里）")
    private String outsideMile;

    /**
     * 车牌所在省
     */
    @Excel(name = "车牌所在省")
    private String licenseProvince;

    /**
     * 车牌所在市
     */
    @Excel(name = "车牌所在市")
    private String licenseCity;

    /**
     * 过户次数
     */
    @Excel(name = "过户次数")
    private Integer transferNum;

    /**
     * 外观
     */
    @Excel(name = "外观")
    private String outColor;

    /**
     * 内饰
     */
    @Excel(name = "内饰")
    private String inColor;

    /**
     * 亮点配置
     */
    @Excel(name = "亮点配置")
    private String highlightsConfiguration;

    /**
     * 钥匙数量
     */
    @Excel(name = "钥匙数量")
    private Integer keyNum;

    /**
     * 年检到期时间
     */
    @Excel(name = "年检到期时间")
    private String inspectAnnuallyTime;

    /**
     * 定金金币数量
     */
    @Excel(name = "定金金币数量")
    private Integer depositPrice;

    /**
     * 0:待上架 1:已上架
     */
    @Excel(name = "0:待上架 1:已上架")
    private Integer status;

    /**
     * 0未删除 1已删除
     */
    @Excel(name = "0未删除 1已删除")
    private Integer deleteFlag;

}
