package yh.platform.modular.carsourceold.param;

import lombok.Data;
import yh.platform.core.pojo.base.param.BaseParam;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
* 二手车车源参数类
 *
 * @author gyp
 * @date 2023-08-03 22:48:06
*/
@Data
public class CarSourceOldPicAddParam {

    /**
     * 编号
     */
    private String uid;

    /**
     * 文件名称
     */
    private String name;

    /**
     * 文件状态
     */
    private String status;

    /**
     * 文件路径
     */
    private String url;

    /**
     * 文件类型
     */
    private Integer type;
}
