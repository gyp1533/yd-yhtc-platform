package yh.platform.modular.carsourceold.param;

import lombok.Data;
import yh.platform.core.pojo.base.param.BaseParam;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
* 二手车车源参数类
 *
 * @author gyp
 * @date 2023-08-03 22:48:06
*/
@Data
public class CarSourceOldAddParam extends BaseParam {

    /**
     *
     */
    @NotNull(message = "不能为空，请检查id参数", groups = {edit.class, delete.class, detail.class})
    private String id;

    /**
     * 车源编号
     */
    @NotBlank(message = "车源编号不能为空，请检查sourceNum参数", groups = {add.class, edit.class})
    private String sourceNum;

    /**
     * 车型id
     */
    @NotBlank(message = "车型id不能为空，请检查modelId参数", groups = {add.class, edit.class})
    private String modelId;

    /**
     * 指导价
     */
    @NotNull(message = "指导价不能为空，请检查guidePrice参数", groups = {add.class, edit.class})
    private BigDecimal guidePrice;

    /**
     * 销售价格
     */
    @NotNull(message = "销售价格不能为空，请检查salePrice参数", groups = {add.class, edit.class})
    private BigDecimal salePrice;

    /**
     * 上牌时间
     */
    @NotBlank(message = "上牌时间不能为空，请检查registrationTime参数", groups = {add.class, edit.class})
    private String registrationTime;

    /**
     * 表显里程（万公里）
     */
    @NotBlank(message = "表显里程（万公里）不能为空，请检查outsideMile参数", groups = {add.class, edit.class})
    private String outsideMile;

    /**
     * 车牌所在省
     */
    @NotBlank(message = "车牌所在省不能为空，请检查licenseProvince参数", groups = {add.class, edit.class})
    private String licenseProvince;

    /**
     * 车牌所在市
     */
    @NotBlank(message = "车牌所在市不能为空，请检查licenseCity参数", groups = {add.class, edit.class})
    private String licenseCity;

    /**
     * 过户次数
     */
    @NotNull(message = "过户次数不能为空，请检查transferNum参数", groups = {add.class, edit.class})
    private Integer transferNum;

    /**
     * 外观
     */
    @NotBlank(message = "外观不能为空，请检查outColor参数", groups = {add.class, edit.class})
    private String outColor;

    /**
     * 内饰
     */
    @NotBlank(message = "内饰不能为空，请检查inColor参数", groups = {add.class, edit.class})
    private String inColor;

    /**
     * 亮点配置
     */
    @NotBlank(message = "亮点配置不能为空，请检查highlightsConfiguration参数", groups = {add.class, edit.class})
    private String highlightsConfiguration;

    /**
     * 钥匙数量
     */
    @NotNull(message = "钥匙数量不能为空，请检查keyNum参数", groups = {add.class, edit.class})
    private Integer keyNum;

    /**
     * 年检到期时间
     */
    @NotBlank(message = "年检到期时间不能为空，请检查inspectAnnuallyTime参数", groups = {add.class, edit.class})
    private String inspectAnnuallyTime;

    /**
     * 定金金币数量
     */
    @NotNull(message = "定金金币数量不能为空，请检查depositPrice参数", groups = {add.class, edit.class})
    private Integer depositPrice;

    private List<CarSourceOldPicAddParam> headList;

    private List<CarSourceOldPicAddParam> frontSeatList;

    private List<CarSourceOldPicAddParam> otherList;

    private List<CarSourceOldPicAddParam> masterControlPaneList;

    private List<CarSourceOldPicAddParam> dashBoardList;

    private List<CarSourceOldPicAddParam> skyLightList;

    private List<CarSourceOldPicAddParam> frontRight45List;

    private List<CarSourceOldPicAddParam> backRight45List;

    private List<CarSourceOldPicAddParam> nameplateList;

}
