package yh.platform.modular.carsourceold.controller;

import yh.platform.core.annotion.BusinessLog;
import yh.platform.core.annotion.Permission;
import yh.platform.core.enums.LogAnnotionOpTypeEnum;
import yh.platform.core.pojo.response.ResponseData;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.core.pojo.response.SuccessResponseData;
import yh.platform.modular.carsourceold.entity.CarSourceOld;
import yh.platform.modular.carsourceold.param.CarSourceOldAddParam;
import yh.platform.modular.carsourceold.param.CarSourceOldParam;
import yh.platform.modular.carsourceold.service.CarSourceOldService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import yh.platform.modular.carsourceold.vo.CarSourceOldVO;

import javax.annotation.Resource;
import java.util.List;

/**
 * 二手车车源控制器
 *
 * @author gyp
 * @date 2023-08-03 22:48:06
 */
@RestController
public class CarSourceOldController {

    @Resource
    private CarSourceOldService carSourceOldService;

    /**
     * 查询二手车车源
     *
     * @author gyp
     * @date 2023-08-03 22:48:06
     */
    @GetMapping("/carSourceOld/page")
    public ResponseData<PageResult<CarSourceOld>> page(CarSourceOldParam carSourceOldParam) {
        PageResult<CarSourceOld> page = carSourceOldService.page(carSourceOldParam);
        return SuccessResponseData.success(page);
    }

    /**
     * 添加二手车车源
     *
     * @author gyp
     * @date 2023-08-03 22:48:06
     */
    @PostMapping("/carSourceOld/add")
    public ResponseData add(@RequestBody @Validated(CarSourceOldAddParam.add.class) CarSourceOldAddParam carSourceOldParam) {
        carSourceOldService.add(carSourceOldParam);
        return SuccessResponseData.success();
    }

    /**
     * 删除二手车车源，可批量删除
     *
     * @author gyp
     * @date 2023-08-03 22:48:06
     */
    @PostMapping("/carSourceOld/delete")
    public void delete(@RequestBody @Validated(CarSourceOldParam.delete.class) List<CarSourceOldParam> carSourceOldParamList) {
            carSourceOldService.delete(carSourceOldParamList);
    }

    /**
     * 编辑二手车车源
     *
     * @author gyp
     * @date 2023-08-03 22:48:06
     */
    @PostMapping("/carSourceOld/edit")
    public void edit(@RequestBody @Validated(CarSourceOldParam.edit.class) CarSourceOldParam carSourceOldParam) {
            carSourceOldService.edit(carSourceOldParam);
    }

    /**
     * 查看二手车车源
     *
     * @author gyp
     * @date 2023-08-03 22:48:06
     */
    @PostMapping("/carSourceOld/detail")
    public CarSourceOldVO detail(@RequestBody @Validated(CarSourceOldParam.detail.class) CarSourceOldParam carSourceOldParam) {
        CarSourceOldVO detail = carSourceOldService.detail(carSourceOldParam);
        return detail;

    }

    /**
     * 二手车车源列表
     *
     * @author gyp
     * @date 2023-08-03 22:48:06
     */
    @PostMapping("/carSourceOld/list")
    public List<CarSourceOld> list(CarSourceOldParam carSourceOldParam) {
        return carSourceOldService.list(carSourceOldParam);
    }

    /**
     * 导出系统用户
     *
     * @author gyp
     * @date 2023-08-03 22:48:06
     */
    @PostMapping("/carSourceOld/export")
    public void export(CarSourceOldParam carSourceOldParam) {
        carSourceOldService.export(carSourceOldParam);
    }

}
