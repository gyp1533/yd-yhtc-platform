package yh.platform.modular.carsourceold.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import yh.platform.modular.carsourceold.entity.CarSourceOld;

/**
 * 二手车车源
 *
 * @author gyp
 * @date 2023-08-03 22:48:06
 */
@Mapper
public interface CarSourceOldMapper extends BaseMapper<CarSourceOld> {
}
