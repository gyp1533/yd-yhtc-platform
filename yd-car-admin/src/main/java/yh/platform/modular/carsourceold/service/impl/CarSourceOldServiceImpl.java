package yh.platform.modular.carsourceold.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.compress.utils.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import yh.platform.core.exception.ServiceException;
import yh.platform.core.factory.PageFactory;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.core.util.PoiUtil;
import yh.platform.modular.carmodel.entity.CarModel;
import yh.platform.modular.carmodel.param.CarModelParam;
import yh.platform.modular.carmodel.service.CarModelService;
import yh.platform.modular.carsourceold.entity.CarSourceOld;
import yh.platform.modular.carsourceold.enums.CarSourceOldExceptionEnum;
import yh.platform.modular.carsourceold.mapper.CarSourceOldMapper;
import yh.platform.modular.carsourceold.param.CarSourceOldAddParam;
import yh.platform.modular.carsourceold.param.CarSourceOldParam;
import yh.platform.modular.carsourceold.param.CarSourceOldPicAddParam;
import yh.platform.modular.carsourceold.service.CarSourceOldService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import yh.platform.modular.carsourceold.vo.CarSourceOldVO;
import yh.platform.modular.carsourceoldfile.entity.CarSourceOldFile;
import yh.platform.modular.carsourceoldfile.service.CarSourceOldFileService;
import yh.platform.modular.spiderarea.entity.SpiderArea;
import yh.platform.modular.spiderarea.param.SpiderAreaParam;
import yh.platform.modular.spiderarea.service.SpiderAreaService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * 二手车车源service接口实现类
 *
 * @author gyp
 * @date 2023-08-03 22:48:06
 */
@Service
public class CarSourceOldServiceImpl extends ServiceImpl<CarSourceOldMapper, CarSourceOld> implements CarSourceOldService {

    @Autowired
    private SpiderAreaService spiderAreaService;

    @Autowired
    private CarModelService carModelService;

    @Autowired
    private CarSourceOldFileService carSourceOldFileService;

    @Override
    public PageResult<CarSourceOld> page(CarSourceOldParam carSourceOldParam) {
        QueryWrapper<CarSourceOld> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(carSourceOldParam)) {

            // 根据车源编号 查询
            if (ObjectUtil.isNotEmpty(carSourceOldParam.getSourceNum())) {
                queryWrapper.lambda().eq(CarSourceOld::getSourceNum, carSourceOldParam.getSourceNum());
            }
            // 根据车型id 查询
            if (ObjectUtil.isNotEmpty(carSourceOldParam.getModelId())) {
                queryWrapper.lambda().eq(CarSourceOld::getModelId, carSourceOldParam.getModelId());
            }
            // 根据指导价 查询
            if (ObjectUtil.isNotEmpty(carSourceOldParam.getGuidePrice())) {
                queryWrapper.lambda().eq(CarSourceOld::getGuidePrice, carSourceOldParam.getGuidePrice());
            }
            // 根据销售价格 查询
            if (ObjectUtil.isNotEmpty(carSourceOldParam.getSalePrice())) {
                queryWrapper.lambda().eq(CarSourceOld::getSalePrice, carSourceOldParam.getSalePrice());
            }
            // 根据上牌时间 查询
            if (ObjectUtil.isNotEmpty(carSourceOldParam.getRegistrationTime())) {
                queryWrapper.lambda().eq(CarSourceOld::getRegistrationTime, carSourceOldParam.getRegistrationTime());
            }
            // 根据表显里程（万公里） 查询
            if (ObjectUtil.isNotEmpty(carSourceOldParam.getOutsideMile())) {
                queryWrapper.lambda().eq(CarSourceOld::getOutsideMile, carSourceOldParam.getOutsideMile());
            }
            // 根据车牌所在省 查询
            if (ObjectUtil.isNotEmpty(carSourceOldParam.getLicenseProvince())) {
                queryWrapper.lambda().eq(CarSourceOld::getLicenseProvince, carSourceOldParam.getLicenseProvince());
            }
            // 根据车牌所在市 查询
            if (ObjectUtil.isNotEmpty(carSourceOldParam.getLicenseCity())) {
                queryWrapper.lambda().eq(CarSourceOld::getLicenseCity, carSourceOldParam.getLicenseCity());
            }
            // 根据过户次数 查询
            if (ObjectUtil.isNotEmpty(carSourceOldParam.getTransferNum())) {
                queryWrapper.lambda().eq(CarSourceOld::getTransferNum, carSourceOldParam.getTransferNum());
            }
            // 根据外观 查询
            if (ObjectUtil.isNotEmpty(carSourceOldParam.getOutColor())) {
                queryWrapper.lambda().eq(CarSourceOld::getOutColor, carSourceOldParam.getOutColor());
            }
            // 根据内饰 查询
            if (ObjectUtil.isNotEmpty(carSourceOldParam.getInColor())) {
                queryWrapper.lambda().eq(CarSourceOld::getInColor, carSourceOldParam.getInColor());
            }
            // 根据亮点配置 查询
            if (ObjectUtil.isNotEmpty(carSourceOldParam.getHighlightsConfiguration())) {
                queryWrapper.lambda().eq(CarSourceOld::getHighlightsConfiguration, carSourceOldParam.getHighlightsConfiguration());
            }
            // 根据钥匙数量 查询
            if (ObjectUtil.isNotEmpty(carSourceOldParam.getKeyNum())) {
                queryWrapper.lambda().eq(CarSourceOld::getKeyNum, carSourceOldParam.getKeyNum());
            }
            // 根据年检到期时间 查询
            if (ObjectUtil.isNotEmpty(carSourceOldParam.getInspectAnnuallyTime())) {
                queryWrapper.lambda().eq(CarSourceOld::getInspectAnnuallyTime, carSourceOldParam.getInspectAnnuallyTime());
            }
            // 根据定金金币数量 查询
            if (ObjectUtil.isNotEmpty(carSourceOldParam.getDepositPrice())) {
                queryWrapper.lambda().eq(CarSourceOld::getDepositPrice, carSourceOldParam.getDepositPrice());
            }
        }
        return new PageResult<>(this.page(PageFactory.defaultPage(), queryWrapper));
    }

    @Override
    public List<CarSourceOld> list(CarSourceOldParam carSourceOldParam) {
        return this.list();
    }

    @Override
    public boolean add(CarSourceOldAddParam addParam) {
        CarSourceOld carSourceOld = new CarSourceOld();
        BeanUtil.copyProperties(addParam, carSourceOld);
        boolean save = this.save(carSourceOld);

        /* 存储图片 */
        List<CarSourceOldPicAddParam> carSourcePicAddParams = mergeList(addParam.getHeadList(), addParam.getFrontSeatList(), addParam.getOtherList(), addParam.getMasterControlPaneList(),
                addParam.getDashBoardList(), addParam.getSkyLightList(), addParam.getFrontRight45List(), addParam.getBackRight45List(),
                addParam.getNameplateList());
        List<CarSourceOldFile> picList = Lists.newArrayList();
        for (CarSourceOldPicAddParam carSourcePicAddParam : carSourcePicAddParams){
            CarSourceOldFile carSourcePic = new CarSourceOldFile();
            carSourcePic.setSourceId(carSourceOld.getId());
            carSourcePic.setFileUrl(carSourcePicAddParam.getUrl());
            carSourcePic.setType(carSourcePicAddParam.getType());
            picList.add(carSourcePic);
        }
        carSourceOldFileService.saveBatch(picList);

        return save;
    }

    /**
     * 合并集合
     * @param list
     * @param <T>
     * @return
     */
    private <T> List<T> mergeList(List<T>... list){
        List<T> res = new ArrayList<>();
        if (Objects.nonNull(list)) {
            for (List<T> l : list) {
                res.addAll(l);
            }
        }
        return res;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<CarSourceOldParam> carSourceOldParamList) {
        carSourceOldParamList.forEach(carSourceOldParam -> {
            this.removeById(carSourceOldParam.getId());
        });
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(CarSourceOldParam carSourceOldParam) {
        CarSourceOld carSourceOld = this.queryCarSourceOld(carSourceOldParam);
        BeanUtil.copyProperties(carSourceOldParam, carSourceOld);
        this.updateById(carSourceOld);
    }

    @Override
    public CarSourceOldVO detail(CarSourceOldParam carSourceOldParam) {
        CarSourceOldVO carSourceOldVO = new CarSourceOldVO();
        CarSourceOld carSourceOld = this.queryCarSourceOld(carSourceOldParam);
        BeanUtil.copyProperties(carSourceOld, carSourceOldVO);
        SpiderAreaParam spiderAreaParam = new SpiderAreaParam();
        spiderAreaParam.setParentCode(carSourceOldVO.getLicenseProvince());
        List<SpiderArea> proviceList = spiderAreaService.list(spiderAreaParam);
        carSourceOldVO.setCityList(proviceList);

        // 获取车型信息
        CarModelParam carModelParam = new CarModelParam();
        carModelParam.setId(carSourceOld.getModelId());
        CarModel detail = carModelService.detail(carModelParam);
        carSourceOldVO.setBrandId(detail.getBrandId());
        carSourceOldVO.setBrandName(detail.getBrandName());
        carSourceOldVO.setSeriesId(detail.getSeriesId());
        carSourceOldVO.setSeriesName(detail.getSeriesName());
        carSourceOldVO.setModelId(detail.getId());
        carSourceOldVO.setModelName(detail.getModelName());

        return carSourceOldVO;
    }

    /**
     * 获取二手车车源
     *
     * @author gyp
     * @date 2023-08-03 22:48:06
     */
    private CarSourceOld queryCarSourceOld(CarSourceOldParam carSourceOldParam) {
        CarSourceOld carSourceOld = this.getById(carSourceOldParam.getId());
        if (ObjectUtil.isNull(carSourceOld)) {
            throw new ServiceException(CarSourceOldExceptionEnum.NOT_EXIST);
        }
        return carSourceOld;
    }

    @Override
    public void export(CarSourceOldParam carSourceOldParam) {
        List<CarSourceOld> list = this.list(carSourceOldParam);
        PoiUtil.exportExcelWithStream("CarSourceOld.xls", CarSourceOld.class, list);
    }

    public static void main(String[] args) {

        //接收输入的日期格式
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        try {
            String s = "2022-12-05T00:00:00Z";
            LocalDateTime parse = LocalDateTime.parse(s, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'"));
            System.out.println(parse);
        } catch (Exception e) {
            System.out.println("格式输入错误，请重新输入");
            e.printStackTrace();
        }
    }

}
