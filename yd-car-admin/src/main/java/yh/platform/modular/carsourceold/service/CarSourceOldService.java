package yh.platform.modular.carsourceold.service;

import com.baomidou.mybatisplus.extension.service.IService;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.modular.carsourceold.entity.CarSourceOld;
import yh.platform.modular.carsourceold.param.CarSourceOldAddParam;
import yh.platform.modular.carsourceold.param.CarSourceOldParam;
import yh.platform.modular.carsourceold.vo.CarSourceOldVO;

import java.util.List;

/**
 * 二手车车源service接口
 *
 * @author gyp
 * @date 2023-08-03 22:48:06
 */
public interface CarSourceOldService extends IService<CarSourceOld> {

    /**
     * 查询二手车车源
     *
     * @author gyp
     * @date 2023-08-03 22:48:06
     */
    PageResult<CarSourceOld> page(CarSourceOldParam carSourceOldParam);

    /**
     * 二手车车源列表
     *
     * @author gyp
     * @date 2023-08-03 22:48:06
     */
    List<CarSourceOld> list(CarSourceOldParam carSourceOldParam);

    /**
     * 添加二手车车源
     *
     * @author gyp
     * @date 2023-08-03 22:48:06
     */
    boolean add(CarSourceOldAddParam carSourceOldParam);

    /**
     * 删除二手车车源
     *
     * @author gyp
     * @date 2023-08-03 22:48:06
     */
    void delete(List<CarSourceOldParam> carSourceOldParamList);

    /**
     * 编辑二手车车源
     *
     * @author gyp
     * @date 2023-08-03 22:48:06
     */
    void edit(CarSourceOldParam carSourceOldParam);

    /**
     * 查看二手车车源
     *
     * @author gyp
     * @date 2023-08-03 22:48:06
     */
    CarSourceOldVO detail(CarSourceOldParam carSourceOldParam);

    /**
     * 导出二手车车源
     *
     * @author gyp
     * @date 2023-08-03 22:48:06
     */
     void export(CarSourceOldParam carSourceOldParam);

}
