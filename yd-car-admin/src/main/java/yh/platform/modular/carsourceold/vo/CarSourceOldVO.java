package yh.platform.modular.carsourceold.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import yh.platform.core.pojo.base.entity.BaseEntity;
import yh.platform.modular.spiderarea.entity.SpiderArea;

import java.math.BigDecimal;
import java.util.List;

/**
 * 二手车车源
 *
 * @author gyp
 * @date 2023-08-03 22:48:06
 */
@Data
public class CarSourceOldVO {

    /**
     *
     */
    private String id;

    /**
     * 车源编号
     */
    private String sourceNum;



    /**
     * 品牌id
     */
    @Excel(name = "品牌id")
    private Long brandId;

    /**
     * 品牌名称
     */
    @Excel(name = "品牌名称")
    private String brandName;

    /**
     * 车系id
     */
    @Excel(name = "车系id")
    private Long seriesId;

    /**
     * 车系名称
     */
    @Excel(name = "车系名称")
    private String seriesName;

    /**
     * 车型id
     */
    private Long modelId;

    /**
     * 车型名称
     */
    @Excel(name = "车型名称")
    private String modelName;

    /**
     * 指导价
     */
    private BigDecimal guidePrice;

    /**
     * 销售价格
     */
    private BigDecimal salePrice;

    /**
     * 上牌时间
     */
    private String registrationTime;

    /**
     * 表显里程（万公里）
     */
    private String outsideMile;

    /**
     * 车牌所在省
     */
    private String licenseProvince;

    /**
     * 车牌所在市
     */
    private String licenseCity;

    /**
     * 过户次数
     */
    private Integer transferNum;

    /**
     * 外观
     */
    private String outColor;

    /**
     * 内饰
     */
    private String inColor;

    /**
     * 亮点配置
     */
    private String highlightsConfiguration;

    /**
     * 钥匙数量
     */
    private Integer keyNum;

    /**
     * 年检到期时间
     */
    private String inspectAnnuallyTime;

    /**
     * 定金金币数量
     */
    private Integer depositPrice;

    /**
     * 0:待上架 1:已上架
     */
    private Integer status;

    /**
     * 0未删除 1已删除
     */
    private Integer deleteFlag;

    /**
     * 获取省份列表
     */
    private List<SpiderArea> cityList;

}
