package yh.platform.modular.carintegralinfo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.modular.carintegralinfo.entity.CarIntegralInfo;
import yh.platform.modular.carintegralinfo.param.CarIntegralInfoParam;
import java.util.List;

/**
 * 积分记录表service接口
 *
 * @author gyp
 * @date 2023-07-28 10:13:54
 */
public interface CarIntegralInfoService extends IService<CarIntegralInfo> {

    /**
     * 查询积分记录表
     *
     * @author gyp
     * @date 2023-07-28 10:13:54
     */
    PageResult<CarIntegralInfo> page(CarIntegralInfoParam carIntegralInfoParam);

    /**
     * 积分记录表列表
     *
     * @author gyp
     * @date 2023-07-28 10:13:54
     */
    List<CarIntegralInfo> list(CarIntegralInfoParam carIntegralInfoParam);

    /**
     * 添加积分记录表
     *
     * @author gyp
     * @date 2023-07-28 10:13:54
     */
    void add(CarIntegralInfoParam carIntegralInfoParam);

    /**
     * 删除积分记录表
     *
     * @author gyp
     * @date 2023-07-28 10:13:54
     */
    void delete(List<CarIntegralInfoParam> carIntegralInfoParamList);

    /**
     * 编辑积分记录表
     *
     * @author gyp
     * @date 2023-07-28 10:13:54
     */
    void edit(CarIntegralInfoParam carIntegralInfoParam);

    /**
     * 查看积分记录表
     *
     * @author gyp
     * @date 2023-07-28 10:13:54
     */
     CarIntegralInfo detail(CarIntegralInfoParam carIntegralInfoParam);

    /**
     * 导出积分记录表
     *
     * @author gyp
     * @date 2023-07-28 10:13:54
     */
     void export(CarIntegralInfoParam carIntegralInfoParam);

}
