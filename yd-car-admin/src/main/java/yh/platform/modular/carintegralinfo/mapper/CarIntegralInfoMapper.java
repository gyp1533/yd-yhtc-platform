package yh.platform.modular.carintegralinfo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import yh.platform.modular.carintegralinfo.entity.CarIntegralInfo;

/**
 * 积分记录表
 *
 * @author gyp
 * @date 2023-07-28 10:13:54
 */
public interface CarIntegralInfoMapper extends BaseMapper<CarIntegralInfo> {
}
