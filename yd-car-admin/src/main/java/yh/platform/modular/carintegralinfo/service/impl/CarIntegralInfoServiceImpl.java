package yh.platform.modular.carintegralinfo.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import yh.platform.core.exception.ServiceException;
import yh.platform.core.factory.PageFactory;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.core.util.PoiUtil;
import yh.platform.modular.appuser.entity.AppUser;
import yh.platform.modular.appuser.service.AppUserService;
import yh.platform.modular.carintegralinfo.entity.CarIntegralInfo;
import yh.platform.modular.carintegralinfo.enums.CarIntegralInfoExceptionEnum;
import yh.platform.modular.carintegralinfo.mapper.CarIntegralInfoMapper;
import yh.platform.modular.carintegralinfo.param.CarIntegralInfoParam;
import yh.platform.modular.carintegralinfo.service.CarIntegralInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 积分记录表service接口实现类
 *
 * @author gyp
 * @date 2023-07-28 10:13:54
 */
@Service
public class CarIntegralInfoServiceImpl extends ServiceImpl<CarIntegralInfoMapper, CarIntegralInfo> implements CarIntegralInfoService {

    @Autowired
    private AppUserService appUserService;

    @Override
    public PageResult<CarIntegralInfo> page(CarIntegralInfoParam carIntegralInfoParam) {
        QueryWrapper<CarIntegralInfo> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(carIntegralInfoParam)) {

            // 根据用户id 查询
            if (ObjectUtil.isNotEmpty(carIntegralInfoParam.getUserId())) {
                queryWrapper.lambda().eq(CarIntegralInfo::getUserId, carIntegralInfoParam.getUserId());
            }
            // 根据积分数量 查询
            if (ObjectUtil.isNotEmpty(carIntegralInfoParam.getIntegralNum())) {
                queryWrapper.lambda().eq(CarIntegralInfo::getIntegralNum, carIntegralInfoParam.getIntegralNum());
            }
            // 根据1:充值2:消费 查询
            if (ObjectUtil.isNotEmpty(carIntegralInfoParam.getType())) {
                queryWrapper.lambda().eq(CarIntegralInfo::getType, carIntegralInfoParam.getType());
            }
            // 根据管理订单id 查询
            if (ObjectUtil.isNotEmpty(carIntegralInfoParam.getOrderId())) {
                queryWrapper.lambda().eq(CarIntegralInfo::getOrderId, carIntegralInfoParam.getOrderId());
            }
            // 根据0未删除 1已删除 查询
            if (ObjectUtil.isNotEmpty(carIntegralInfoParam.getDeleteFlag())) {
                queryWrapper.lambda().eq(CarIntegralInfo::getDeleteFlag, carIntegralInfoParam.getDeleteFlag());
            }
            // 根据备注 查询
            if (ObjectUtil.isNotEmpty(carIntegralInfoParam.getRemark())) {
                queryWrapper.lambda().eq(CarIntegralInfo::getRemark, carIntegralInfoParam.getRemark());
            }
        }
        return new PageResult<>(this.page(PageFactory.defaultPage(), queryWrapper));
    }

    @Override
    public List<CarIntegralInfo> list(CarIntegralInfoParam carIntegralInfoParam) {
        return this.list();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void add(CarIntegralInfoParam carIntegralInfoParam) {

        CarIntegralInfo carIntegralInfo = new CarIntegralInfo();
        BeanUtil.copyProperties(carIntegralInfoParam, carIntegralInfo);
        this.save(carIntegralInfo);

        /* 更新积分数量 */
        AppUser appUser = appUserService.getById(carIntegralInfoParam.getUserId());
        appUser.setIntegral(appUser.getIntegral()+carIntegralInfoParam.getIntegralNum());
        appUserService.updateById(appUser);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<CarIntegralInfoParam> carIntegralInfoParamList) {
        carIntegralInfoParamList.forEach(carIntegralInfoParam -> {
            this.removeById(carIntegralInfoParam.getId());
        });
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(CarIntegralInfoParam carIntegralInfoParam) {
        CarIntegralInfo carIntegralInfo = this.queryCarIntegralInfo(carIntegralInfoParam);
        BeanUtil.copyProperties(carIntegralInfoParam, carIntegralInfo);
        this.updateById(carIntegralInfo);
    }

    @Override
    public CarIntegralInfo detail(CarIntegralInfoParam carIntegralInfoParam) {
        return this.queryCarIntegralInfo(carIntegralInfoParam);
    }

    /**
     * 获取积分记录表
     *
     * @author gyp
     * @date 2023-07-28 10:13:54
     */
    private CarIntegralInfo queryCarIntegralInfo(CarIntegralInfoParam carIntegralInfoParam) {
        CarIntegralInfo carIntegralInfo = this.getById(carIntegralInfoParam.getId());
        if (ObjectUtil.isNull(carIntegralInfo)) {
            throw new ServiceException(CarIntegralInfoExceptionEnum.NOT_EXIST);
        }
        return carIntegralInfo;
    }

    @Override
    public void export(CarIntegralInfoParam carIntegralInfoParam) {
        List<CarIntegralInfo> list = this.list(carIntegralInfoParam);
        PoiUtil.exportExcelWithStream("CarIntegralInfo.xls", CarIntegralInfo.class, list);
    }

}
