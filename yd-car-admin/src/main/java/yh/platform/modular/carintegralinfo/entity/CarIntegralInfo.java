package yh.platform.modular.carintegralinfo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.afterturn.easypoi.excel.annotation.Excel;
import yh.platform.core.pojo.base.entity.BaseEntity;

/**
 * 积分记录表
 *
 * @author gyp
 * @date 2023-07-28 10:13:54
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("car_integral_info")
public class CarIntegralInfo extends BaseEntity {

    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 用户id
     */
    @Excel(name = "用户id")
    private String userId;

    /**
     * 积分数量
     */
    @Excel(name = "积分数量")
    private Integer integralNum;

    /**
     * 1:充值2:消费
     */
    @Excel(name = "1:充值2:消费")
    private Integer type;

    /**
     * 管理订单id
     */
    @Excel(name = "管理订单id")
    private String orderId;

    /**
     * 0未删除 1已删除
     */
    @Excel(name = "0未删除 1已删除")
    private Integer deleteFlag;

    /**
     * 备注
     */
    @Excel(name = "备注")
    private String remark;

}
