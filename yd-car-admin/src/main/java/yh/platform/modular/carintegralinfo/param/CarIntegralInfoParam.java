package yh.platform.modular.carintegralinfo.param;

import yh.platform.core.pojo.base.param.BaseParam;
import lombok.Data;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

/**
* 积分记录表参数类
 *
 * @author gyp
 * @date 2023-07-28 10:13:54
*/
@Data
public class CarIntegralInfoParam extends BaseParam {

    /**
     * id
     */
    @NotNull(message = "id不能为空，请检查id参数", groups = {edit.class, delete.class, detail.class})
    private String id;

    /**
     * 用户id
     */
    @NotBlank(message = "用户id不能为空，请检查userId参数", groups = {add.class, edit.class})
    private String userId;

    /**
     * 积分数量
     */
    @NotNull(message = "积分数量不能为空，请检查integralNum参数", groups = {add.class, edit.class})
    private Integer integralNum;

    /**
     * 1:充值2:消费
     */
    @NotNull(message = "1:充值2:消费不能为空，请检查type参数", groups = {add.class, edit.class})
    private Integer type;

    /**
     * 管理订单id
     */
    @NotBlank(message = "管理订单id不能为空，请检查orderId参数")
    private String orderId;

    /**
     * 0未删除 1已删除
     */
    @NotNull(message = "0未删除 1已删除不能为空，请检查deleteFlag参数")
    private Integer deleteFlag;

    /**
     * 备注
     */
    @NotBlank(message = "备注不能为空，请检查remark参数", groups = {add.class, edit.class})
    private String remark;

}
