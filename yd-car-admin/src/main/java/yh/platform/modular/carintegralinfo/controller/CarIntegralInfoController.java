package yh.platform.modular.carintegralinfo.controller;

import yh.platform.core.annotion.BusinessLog;
import yh.platform.core.annotion.Permission;
import yh.platform.core.enums.LogAnnotionOpTypeEnum;
import yh.platform.core.pojo.response.ResponseData;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.core.pojo.response.SuccessResponseData;
import yh.platform.modular.carintegralinfo.entity.CarIntegralInfo;
import yh.platform.modular.carintegralinfo.param.CarIntegralInfoParam;
import yh.platform.modular.carintegralinfo.service.CarIntegralInfoService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 积分记录表控制器
 *
 * @author gyp
 * @date 2023-07-28 10:13:54
 */
@RestController
public class CarIntegralInfoController {

    @Resource
    private CarIntegralInfoService carIntegralInfoService;

    /**
     * 查询积分记录表
     *
     * @author gyp
     * @date 2023-07-28 10:13:54
     */
    @GetMapping("/carIntegralInfo/page")
    public ResponseData<PageResult<CarIntegralInfo>> page(CarIntegralInfoParam carIntegralInfoParam) {
        return SuccessResponseData.success(carIntegralInfoService.page(carIntegralInfoParam));
    }

    /**
     * 添加积分记录表
     *
     * @author gyp
     * @date 2023-07-28 10:13:54
     */
    @PostMapping("/carIntegralInfo/add")
    public ResponseData add(@RequestBody @Validated(CarIntegralInfoParam.add.class) CarIntegralInfoParam carIntegralInfoParam) {
        carIntegralInfoService.add(carIntegralInfoParam);
		return SuccessResponseData.success();
    }

    /**
     * 删除积分记录表，可批量删除
     *
     * @author gyp
     * @date 2023-07-28 10:13:54
     */
    @PostMapping("/carIntegralInfo/delete")
    public ResponseData delete(@RequestBody @Validated(CarIntegralInfoParam.delete.class) List<CarIntegralInfoParam> carIntegralInfoParamList) {
        carIntegralInfoService.delete(carIntegralInfoParamList);
		return SuccessResponseData.success();
    }

    /**
     * 编辑积分记录表
     *
     * @author gyp
     * @date 2023-07-28 10:13:54
     */
    @PostMapping("/carIntegralInfo/edit")
    public ResponseData edit(@RequestBody @Validated(CarIntegralInfoParam.edit.class) CarIntegralInfoParam carIntegralInfoParam) {
        carIntegralInfoService.edit(carIntegralInfoParam);
		return SuccessResponseData.success();
    }

    /**
     * 查看积分记录表
     *
     * @author gyp
     * @date 2023-07-28 10:13:54
     */
    @PostMapping("/carIntegralInfo/detail")
    public ResponseData<CarIntegralInfo> detail(@RequestBody @Validated(CarIntegralInfoParam.detail.class) CarIntegralInfoParam carIntegralInfoParam) {

        return SuccessResponseData.success(carIntegralInfoService.detail(carIntegralInfoParam));
    }

    /**
     * 积分记录表列表
     *
     * @author gyp
     * @date 2023-07-28 10:13:54
     */
    @PostMapping("/carIntegralInfo/list")
    public ResponseData<List<CarIntegralInfo>> list(CarIntegralInfoParam carIntegralInfoParam) {
        return SuccessResponseData.success(carIntegralInfoService.list(carIntegralInfoParam));
    }

    /**
     * 导出系统用户
     *
     * @author gyp
     * @date 2023-07-28 10:13:54
     */
    @PostMapping("/carIntegralInfo/export")
    public void export(CarIntegralInfoParam carIntegralInfoParam) {
        carIntegralInfoService.export(carIntegralInfoParam);
    }

}
