package yh.platform.modular.file.controlle;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import yh.platform.core.pojo.response.ResponseData;
import yh.platform.core.util.OssUtil;
import yh.platform.modular.file.vo.UploadFileVO;

import java.io.IOException;


@Slf4j
@RestController
public class FileController {

    @Autowired
    private OssUtil ossUtil;

    @PostMapping("/car/uploadFile")
    public ResponseData<UploadFileVO> uploadFile(@RequestParam("file") MultipartFile file,
                                                 @RequestParam("fileType") Integer fileType) throws IOException {
        String fileUrl = ossUtil.uploadFile(file, fileType);
        UploadFileVO uploadFileVO = new UploadFileVO(fileUrl);
        return ResponseData.success(uploadFileVO);
    }

}
