package yh.platform.modular.file.vo;

import lombok.Data;

@Data
public class UploadFileVO {

    public UploadFileVO(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    /**
     * 文件地址
     */
    private String fileUrl;

}
