package yh.platform.modular.spiderarea.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.afterturn.easypoi.excel.annotation.Excel;
import yh.platform.core.pojo.base.entity.BaseEntity;

/**
 * 爬虫区域信息表
 *
 * @author gyp
 * @date 2023-07-30 08:11:52
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("spider_area")
public class SpiderArea extends BaseEntity {

    /**
     * 
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Integer id;

    /**
     * 区划代码
     */
    @Excel(name = "区划代码")
    private String code;

    /**
     * 区域名称
     */
    @Excel(name = "区域名称")
    private String name;

    /**
     * 父级区划代码
     */
    @Excel(name = "父级区划代码")
    private String parentCode;

    /**
     * 1:省份、直辖市；2：城市；3:区县；4：乡镇；5：街道
     */
    @Excel(name = "1:省份、直辖市；2：城市；3:区县；4：乡镇；5：街道")
    private Integer grade;

    /**
     * 历史名称
     */
    @Excel(name = "历史名称")
    private String hisName;

    /**
     * 历史区划代码
     */
    @Excel(name = "历史区划代码")
    private String hisCode;

    /**
     * 纬度
     */
    @Excel(name = "纬度")
    private String lat;

    /**
     * 精度
     */
    @Excel(name = "精度")
    private String lng;

}
