package yh.platform.modular.spiderarea.param;

import yh.platform.core.pojo.base.param.BaseParam;
import lombok.Data;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

/**
* 爬虫区域信息表参数类
 *
 * @author gyp
 * @date 2023-07-30 08:11:52
*/
@Data
public class SpiderAreaParam extends BaseParam {

    /**
     * 
     */
    @NotNull(message = "不能为空，请检查id参数", groups = {edit.class, delete.class, detail.class})
    private Integer id;

    /**
     * 区划代码
     */
    @NotBlank(message = "区划代码不能为空，请检查code参数", groups = {add.class, edit.class})
    private String code;

    /**
     * 区域名称
     */
    @NotBlank(message = "区域名称不能为空，请检查name参数", groups = {add.class, edit.class})
    private String name;

    /**
     * 父级区划代码
     */
    @NotBlank(message = "父级区划代码不能为空，请检查parentCode参数", groups = {add.class, edit.class})
    private String parentCode;

    /**
     * 1:省份、直辖市；2：城市；3:区县；4：乡镇；5：街道
     */
    @NotNull(message = "1:省份、直辖市；2：城市；3:区县；4：乡镇；5：街道不能为空，请检查grade参数", groups = {add.class, edit.class})
    private Integer grade;

    /**
     * 历史名称
     */
    @NotBlank(message = "历史名称不能为空，请检查hisName参数", groups = {add.class, edit.class})
    private String hisName;

    /**
     * 历史区划代码
     */
    @NotBlank(message = "历史区划代码不能为空，请检查hisCode参数", groups = {add.class, edit.class})
    private String hisCode;

    /**
     * 纬度
     */
    @NotBlank(message = "纬度不能为空，请检查lat参数", groups = {add.class, edit.class})
    private String lat;

    /**
     * 精度
     */
    @NotBlank(message = "精度不能为空，请检查lng参数", groups = {add.class, edit.class})
    private String lng;

}
