package yh.platform.modular.spiderarea.controller;

import yh.platform.core.annotion.BusinessLog;
import yh.platform.core.annotion.Permission;
import yh.platform.core.enums.LogAnnotionOpTypeEnum;
import yh.platform.core.pojo.response.ResponseData;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.core.pojo.response.SuccessResponseData;
import yh.platform.modular.spiderarea.entity.SpiderArea;
import yh.platform.modular.spiderarea.param.SpiderAreaParam;
import yh.platform.modular.spiderarea.service.SpiderAreaService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 爬虫区域信息表控制器
 *
 * @author gyp
 * @date 2023-07-30 08:11:52
 */
@RestController
public class SpiderAreaController {

    @Resource
    private SpiderAreaService spiderAreaService;

    /**
     * 爬虫区域信息表列表
     * @author gyp
     * @date 2023-07-30 08:11:52
     */
    @GetMapping("/area/spiderArea/list")
    public ResponseData<List<SpiderArea>> list(SpiderAreaParam spiderAreaParam) {
        return SuccessResponseData.success(spiderAreaService.list(spiderAreaParam));
    }


}
