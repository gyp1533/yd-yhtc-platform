package yh.platform.modular.spiderarea.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import yh.platform.core.exception.ServiceException;
import yh.platform.core.factory.PageFactory;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.core.util.PoiUtil;
import yh.platform.modular.spiderarea.entity.SpiderArea;
import yh.platform.modular.spiderarea.enums.SpiderAreaExceptionEnum;
import yh.platform.modular.spiderarea.mapper.SpiderAreaMapper;
import yh.platform.modular.spiderarea.param.SpiderAreaParam;
import yh.platform.modular.spiderarea.service.SpiderAreaService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 爬虫区域信息表service接口实现类
 *
 * @author gyp
 * @date 2023-07-30 08:11:52
 */
@Service
public class SpiderAreaServiceImpl extends ServiceImpl<SpiderAreaMapper, SpiderArea> implements SpiderAreaService {

    @Override
    public PageResult<SpiderArea> page(SpiderAreaParam spiderAreaParam) {
        QueryWrapper<SpiderArea> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(spiderAreaParam)) {

            // 根据区划代码 查询
            if (ObjectUtil.isNotEmpty(spiderAreaParam.getCode())) {
                queryWrapper.lambda().eq(SpiderArea::getCode, spiderAreaParam.getCode());
            }
            // 根据区域名称 查询
            if (ObjectUtil.isNotEmpty(spiderAreaParam.getName())) {
                queryWrapper.lambda().eq(SpiderArea::getName, spiderAreaParam.getName());
            }
            // 根据父级区划代码 查询
            if (ObjectUtil.isNotEmpty(spiderAreaParam.getParentCode())) {
                queryWrapper.lambda().eq(SpiderArea::getParentCode, spiderAreaParam.getParentCode());
            }
            // 根据1:省份、直辖市；2：城市；3:区县；4：乡镇；5：街道 查询
            if (ObjectUtil.isNotEmpty(spiderAreaParam.getGrade())) {
                queryWrapper.lambda().eq(SpiderArea::getGrade, spiderAreaParam.getGrade());
            }
            // 根据历史名称 查询
            if (ObjectUtil.isNotEmpty(spiderAreaParam.getHisName())) {
                queryWrapper.lambda().eq(SpiderArea::getHisName, spiderAreaParam.getHisName());
            }
            // 根据历史区划代码 查询
            if (ObjectUtil.isNotEmpty(spiderAreaParam.getHisCode())) {
                queryWrapper.lambda().eq(SpiderArea::getHisCode, spiderAreaParam.getHisCode());
            }
            // 根据纬度 查询
            if (ObjectUtil.isNotEmpty(spiderAreaParam.getLat())) {
                queryWrapper.lambda().eq(SpiderArea::getLat, spiderAreaParam.getLat());
            }
            // 根据精度 查询
            if (ObjectUtil.isNotEmpty(spiderAreaParam.getLng())) {
                queryWrapper.lambda().eq(SpiderArea::getLng, spiderAreaParam.getLng());
            }
        }
        return new PageResult<>(this.page(PageFactory.defaultPage(), queryWrapper));
    }

    @Override
    public List<SpiderArea> list(SpiderAreaParam spiderAreaParam) {
        QueryWrapper<SpiderArea> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(spiderAreaParam)) {

            // 根据区划代码 查询
            if (ObjectUtil.isNotEmpty(spiderAreaParam.getGrade())) {
                queryWrapper.lambda().eq(SpiderArea::getGrade, spiderAreaParam.getGrade());
            }
            // 根据区划代码 查询
            if (ObjectUtil.isNotEmpty(spiderAreaParam.getParentCode())) {
                String parentCode = spiderAreaParam.getParentCode();
                queryWrapper.lambda().eq(SpiderArea::getParentCode, parentCode);
            }
        }
        List<SpiderArea> list = this.list(queryWrapper);
        return list;
    }

    @Override
    public void add(SpiderAreaParam spiderAreaParam) {
        SpiderArea spiderArea = new SpiderArea();
        BeanUtil.copyProperties(spiderAreaParam, spiderArea);
        this.save(spiderArea);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<SpiderAreaParam> spiderAreaParamList) {
        spiderAreaParamList.forEach(spiderAreaParam -> {
            this.removeById(spiderAreaParam.getId());
        });
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SpiderAreaParam spiderAreaParam) {
        SpiderArea spiderArea = this.querySpiderArea(spiderAreaParam);
        BeanUtil.copyProperties(spiderAreaParam, spiderArea);
        this.updateById(spiderArea);
    }

    @Override
    public SpiderArea detail(SpiderAreaParam spiderAreaParam) {
        return this.querySpiderArea(spiderAreaParam);
    }

    /**
     * 获取爬虫区域信息表
     *
     * @author gyp
     * @date 2023-07-30 08:11:52
     */
    private SpiderArea querySpiderArea(SpiderAreaParam spiderAreaParam) {
        SpiderArea spiderArea = this.getById(spiderAreaParam.getId());
        if (ObjectUtil.isNull(spiderArea)) {
            throw new ServiceException(SpiderAreaExceptionEnum.NOT_EXIST);
        }
        return spiderArea;
    }

    @Override
    public void export(SpiderAreaParam spiderAreaParam) {
        List<SpiderArea> list = this.list(spiderAreaParam);
        PoiUtil.exportExcelWithStream("SpiderArea.xls", SpiderArea.class, list);
    }

}
