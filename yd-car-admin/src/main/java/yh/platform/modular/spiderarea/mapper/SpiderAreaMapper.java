package yh.platform.modular.spiderarea.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import yh.platform.modular.spiderarea.entity.SpiderArea;

/**
 * 爬虫区域信息表
 *
 * @author gyp
 * @date 2023-07-30 08:11:52
 */
public interface SpiderAreaMapper extends BaseMapper<SpiderArea> {
}
