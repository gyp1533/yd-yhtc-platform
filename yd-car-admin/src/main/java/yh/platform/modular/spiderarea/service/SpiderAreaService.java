package yh.platform.modular.spiderarea.service;

import com.baomidou.mybatisplus.extension.service.IService;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.modular.spiderarea.entity.SpiderArea;
import yh.platform.modular.spiderarea.param.SpiderAreaParam;
import java.util.List;

/**
 * 爬虫区域信息表service接口
 *
 * @author gyp
 * @date 2023-07-30 08:11:52
 */
public interface SpiderAreaService extends IService<SpiderArea> {

    /**
     * 查询爬虫区域信息表
     *
     * @author gyp
     * @date 2023-07-30 08:11:52
     */
    PageResult<SpiderArea> page(SpiderAreaParam spiderAreaParam);

    /**
     * 爬虫区域信息表列表
     *
     * @author gyp
     * @date 2023-07-30 08:11:52
     */
    List<SpiderArea> list(SpiderAreaParam spiderAreaParam);

    /**
     * 添加爬虫区域信息表
     *
     * @author gyp
     * @date 2023-07-30 08:11:52
     */
    void add(SpiderAreaParam spiderAreaParam);

    /**
     * 删除爬虫区域信息表
     *
     * @author gyp
     * @date 2023-07-30 08:11:52
     */
    void delete(List<SpiderAreaParam> spiderAreaParamList);

    /**
     * 编辑爬虫区域信息表
     *
     * @author gyp
     * @date 2023-07-30 08:11:52
     */
    void edit(SpiderAreaParam spiderAreaParam);

    /**
     * 查看爬虫区域信息表
     *
     * @author gyp
     * @date 2023-07-30 08:11:52
     */
     SpiderArea detail(SpiderAreaParam spiderAreaParam);

    /**
     * 导出爬虫区域信息表
     *
     * @author gyp
     * @date 2023-07-30 08:11:52
     */
     void export(SpiderAreaParam spiderAreaParam);

}
