package yh.platform.modular.carsourceoldfile.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.afterturn.easypoi.excel.annotation.Excel;
import yh.platform.core.pojo.base.entity.BaseEntity;

/**
 * 二手车附件表
 *
 * @author gyp
 * @date 2023-07-30 08:16:30
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("car_source_old_file")
public class CarSourceOldFile extends BaseEntity {

    /**
     * 
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 车源id
     */
    @Excel(name = "车源id")
    private String sourceId;

    /**
     * 1:外观 2:内饰 3:底盘
     */
    @Excel(name = "1:外观 2:内饰 3:底盘")
    private Integer type;

    /**
     * 排序
     */
    @Excel(name = "排序")
    private Integer sort;

    /**
     * 图片地址
     */
    @Excel(name = "图片地址")
    private String fileUrl;

}
