package yh.platform.modular.carsourceoldfile.service;

import com.baomidou.mybatisplus.extension.service.IService;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.modular.carsourceoldfile.entity.CarSourceOldFile;
import yh.platform.modular.carsourceoldfile.param.CarSourceOldFileParam;
import java.util.List;

/**
 * 二手车附件表service接口
 *
 * @author gyp
 * @date 2023-07-30 08:16:30
 */
public interface CarSourceOldFileService extends IService<CarSourceOldFile> {

    /**
     * 查询二手车附件表
     *
     * @author gyp
     * @date 2023-07-30 08:16:30
     */
    PageResult<CarSourceOldFile> page(CarSourceOldFileParam carSourceOldFileParam);

    /**
     * 二手车附件表列表
     *
     * @author gyp
     * @date 2023-07-30 08:16:30
     */
    List<CarSourceOldFile> list(CarSourceOldFileParam carSourceOldFileParam);

    /**
     * 添加二手车附件表
     *
     * @author gyp
     * @date 2023-07-30 08:16:30
     */
    void add(CarSourceOldFileParam carSourceOldFileParam);

    /**
     * 删除二手车附件表
     *
     * @author gyp
     * @date 2023-07-30 08:16:30
     */
    void delete(List<CarSourceOldFileParam> carSourceOldFileParamList);

    /**
     * 编辑二手车附件表
     *
     * @author gyp
     * @date 2023-07-30 08:16:30
     */
    void edit(CarSourceOldFileParam carSourceOldFileParam);

    /**
     * 查看二手车附件表
     *
     * @author gyp
     * @date 2023-07-30 08:16:30
     */
     CarSourceOldFile detail(CarSourceOldFileParam carSourceOldFileParam);

    /**
     * 导出二手车附件表
     *
     * @author gyp
     * @date 2023-07-30 08:16:30
     */
     void export(CarSourceOldFileParam carSourceOldFileParam);

}
