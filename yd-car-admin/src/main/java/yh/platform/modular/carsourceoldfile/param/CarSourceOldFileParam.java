package yh.platform.modular.carsourceoldfile.param;

import yh.platform.core.pojo.base.param.BaseParam;
import lombok.Data;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

/**
* 二手车附件表参数类
 *
 * @author gyp
 * @date 2023-07-30 08:16:30
*/
@Data
public class CarSourceOldFileParam extends BaseParam {

    /**
     * 
     */
    @NotNull(message = "不能为空，请检查id参数", groups = {edit.class, delete.class, detail.class})
    private String id;

    /**
     * 车源id
     */
    @NotBlank(message = "车源id不能为空，请检查sourceId参数", groups = {add.class, edit.class})
    private String sourceId;

    /**
     * 1:外观 2:内饰 3:底盘
     */
    @NotNull(message = "1:外观 2:内饰 3:底盘不能为空，请检查type参数", groups = {add.class, edit.class})
    private Integer type;

    /**
     * 排序
     */
    @NotNull(message = "排序不能为空，请检查sort参数", groups = {add.class, edit.class})
    private Integer sort;

    /**
     * 图片地址
     */
    @NotBlank(message = "图片地址不能为空，请检查fileUrl参数", groups = {add.class, edit.class})
    private String fileUrl;

}
