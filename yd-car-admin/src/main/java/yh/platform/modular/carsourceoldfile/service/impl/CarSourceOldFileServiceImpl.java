package yh.platform.modular.carsourceoldfile.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import yh.platform.core.exception.ServiceException;
import yh.platform.core.factory.PageFactory;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.core.util.PoiUtil;
import yh.platform.modular.carsourceoldfile.entity.CarSourceOldFile;
import yh.platform.modular.carsourceoldfile.enums.CarSourceOldFileExceptionEnum;
import yh.platform.modular.carsourceoldfile.mapper.CarSourceOldFileMapper;
import yh.platform.modular.carsourceoldfile.param.CarSourceOldFileParam;
import yh.platform.modular.carsourceoldfile.service.CarSourceOldFileService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 二手车附件表service接口实现类
 *
 * @author gyp
 * @date 2023-07-30 08:16:30
 */
@Service
public class CarSourceOldFileServiceImpl extends ServiceImpl<CarSourceOldFileMapper, CarSourceOldFile> implements CarSourceOldFileService {

    @Override
    public PageResult<CarSourceOldFile> page(CarSourceOldFileParam carSourceOldFileParam) {
        QueryWrapper<CarSourceOldFile> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(carSourceOldFileParam)) {

            // 根据车源id 查询
            if (ObjectUtil.isNotEmpty(carSourceOldFileParam.getSourceId())) {
                queryWrapper.lambda().eq(CarSourceOldFile::getSourceId, carSourceOldFileParam.getSourceId());
            }
            // 根据1:外观 2:内饰 3:底盘 查询
            if (ObjectUtil.isNotEmpty(carSourceOldFileParam.getType())) {
                queryWrapper.lambda().eq(CarSourceOldFile::getType, carSourceOldFileParam.getType());
            }
            // 根据排序 查询
            if (ObjectUtil.isNotEmpty(carSourceOldFileParam.getSort())) {
                queryWrapper.lambda().eq(CarSourceOldFile::getSort, carSourceOldFileParam.getSort());
            }
            // 根据图片地址 查询
            if (ObjectUtil.isNotEmpty(carSourceOldFileParam.getFileUrl())) {
                queryWrapper.lambda().eq(CarSourceOldFile::getFileUrl, carSourceOldFileParam.getFileUrl());
            }
        }
        return new PageResult<>(this.page(PageFactory.defaultPage(), queryWrapper));
    }

    @Override
    public List<CarSourceOldFile> list(CarSourceOldFileParam carSourceOldFileParam) {
        return this.list();
    }

    @Override
    public void add(CarSourceOldFileParam carSourceOldFileParam) {
        CarSourceOldFile carSourceOldFile = new CarSourceOldFile();
        BeanUtil.copyProperties(carSourceOldFileParam, carSourceOldFile);
        this.save(carSourceOldFile);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<CarSourceOldFileParam> carSourceOldFileParamList) {
        carSourceOldFileParamList.forEach(carSourceOldFileParam -> {
            this.removeById(carSourceOldFileParam.getId());
        });
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(CarSourceOldFileParam carSourceOldFileParam) {
        CarSourceOldFile carSourceOldFile = this.queryCarSourceOldFile(carSourceOldFileParam);
        BeanUtil.copyProperties(carSourceOldFileParam, carSourceOldFile);
        this.updateById(carSourceOldFile);
    }

    @Override
    public CarSourceOldFile detail(CarSourceOldFileParam carSourceOldFileParam) {
        return this.queryCarSourceOldFile(carSourceOldFileParam);
    }

    /**
     * 获取二手车附件表
     *
     * @author gyp
     * @date 2023-07-30 08:16:30
     */
    private CarSourceOldFile queryCarSourceOldFile(CarSourceOldFileParam carSourceOldFileParam) {
        CarSourceOldFile carSourceOldFile = this.getById(carSourceOldFileParam.getId());
        if (ObjectUtil.isNull(carSourceOldFile)) {
            throw new ServiceException(CarSourceOldFileExceptionEnum.NOT_EXIST);
        }
        return carSourceOldFile;
    }

    @Override
    public void export(CarSourceOldFileParam carSourceOldFileParam) {
        List<CarSourceOldFile> list = this.list(carSourceOldFileParam);
        PoiUtil.exportExcelWithStream("CarSourceOldFile.xls", CarSourceOldFile.class, list);
    }

}
