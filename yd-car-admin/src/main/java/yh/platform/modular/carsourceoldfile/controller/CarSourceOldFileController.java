package yh.platform.modular.carsourceoldfile.controller;

import yh.platform.core.annotion.BusinessLog;
import yh.platform.core.annotion.Permission;
import yh.platform.core.enums.LogAnnotionOpTypeEnum;
import yh.platform.core.pojo.response.ResponseData;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.core.pojo.response.SuccessResponseData;
import yh.platform.modular.carsourceoldfile.entity.CarSourceOldFile;
import yh.platform.modular.carsourceoldfile.param.CarSourceOldFileParam;
import yh.platform.modular.carsourceoldfile.service.CarSourceOldFileService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 二手车附件表控制器
 *
 * @author gyp
 * @date 2023-07-30 08:16:30
 */
@RestController
public class CarSourceOldFileController {

    @Resource
    private CarSourceOldFileService carSourceOldFileService;

    /**
     * 查询二手车附件表
     *
     * @author gyp
     * @date 2023-07-30 08:16:30
     */
    @PostMapping("/carSourceOldFile/page")
    public PageResult<CarSourceOldFile> page(CarSourceOldFileParam carSourceOldFileParam) {
        return carSourceOldFileService.page(carSourceOldFileParam);
    }

    /**
     * 添加二手车附件表
     *
     * @author gyp
     * @date 2023-07-30 08:16:30
     */
    @PostMapping("/carSourceOldFile/add")
    public void add(@RequestBody @Validated(CarSourceOldFileParam.add.class) CarSourceOldFileParam carSourceOldFileParam) {
            carSourceOldFileService.add(carSourceOldFileParam);
    }

    /**
     * 删除二手车附件表，可批量删除
     *
     * @author gyp
     * @date 2023-07-30 08:16:30
     */
    @PostMapping("/carSourceOldFile/delete")
    public void delete(@RequestBody @Validated(CarSourceOldFileParam.delete.class) List<CarSourceOldFileParam> carSourceOldFileParamList) {
            carSourceOldFileService.delete(carSourceOldFileParamList);
    }

    /**
     * 编辑二手车附件表
     *
     * @author gyp
     * @date 2023-07-30 08:16:30
     */
    @PostMapping("/carSourceOldFile/edit")
    public void edit(@RequestBody @Validated(CarSourceOldFileParam.edit.class) CarSourceOldFileParam carSourceOldFileParam) {
            carSourceOldFileService.edit(carSourceOldFileParam);
    }

    /**
     * 查看二手车附件表
     *
     * @author gyp
     * @date 2023-07-30 08:16:30
     */
    @PostMapping("/carSourceOldFile/detail")
    public CarSourceOldFile detail(@Validated(CarSourceOldFileParam.detail.class) CarSourceOldFileParam carSourceOldFileParam) {

        return carSourceOldFileService.detail(carSourceOldFileParam);
    }

    /**
     * 二手车附件表列表
     *
     * @author gyp
     * @date 2023-07-30 08:16:30
     */
    @PostMapping("/carSourceOldFile/list")
    public List<CarSourceOldFile> list(CarSourceOldFileParam carSourceOldFileParam) {
        return carSourceOldFileService.list(carSourceOldFileParam);
    }

    /**
     * 导出系统用户
     *
     * @author gyp
     * @date 2023-07-30 08:16:30
     */
    @PostMapping("/carSourceOldFile/export")
    public void export(CarSourceOldFileParam carSourceOldFileParam) {
        carSourceOldFileService.export(carSourceOldFileParam);
    }

}
