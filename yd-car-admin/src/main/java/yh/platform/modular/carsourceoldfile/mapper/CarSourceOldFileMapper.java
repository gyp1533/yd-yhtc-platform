package yh.platform.modular.carsourceoldfile.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import yh.platform.modular.carsourceoldfile.entity.CarSourceOldFile;

/**
 * 二手车附件表
 *
 * @author gyp
 * @date 2023-07-30 08:16:30
 */
public interface CarSourceOldFileMapper extends BaseMapper<CarSourceOldFile> {
}
