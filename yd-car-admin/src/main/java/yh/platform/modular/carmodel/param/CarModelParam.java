package yh.platform.modular.carmodel.param;

import yh.platform.core.pojo.base.param.BaseParam;
import lombok.Data;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

/**
* 车型表参数类
 *
 * @author gyp
 * @date 2023-07-24 18:40:10
*/
@Data
public class CarModelParam extends BaseParam {

    /**
     *
     */
    @NotNull(message = "不能为空，请检查id参数", groups = {edit.class, delete.class, detail.class})
    private Long id;

    /**
     * 汽车之家车型id
     */
    @NotNull(message = "汽车之家车型id不能为空，请检查qczjModelId参数", groups = {add.class, edit.class})
    private Long qczjModelId;

    /**
     * 品牌id
     */
    @NotNull(message = "品牌id不能为空，请检查brandId参数", groups = {add.class, edit.class})
    private Long brandId;

    /**
     * 品牌名称
     */
    @NotBlank(message = "品牌名称不能为空，请检查brandName参数", groups = {add.class, edit.class})
    private String brandName;

    /**
     * 车系id
     */
    @NotNull(message = "车系id不能为空，请检查seriesId参数", groups = {add.class, edit.class})
    private Long seriesId;

    /**
     * 车系名称
     */
    @NotBlank(message = "车系名称不能为空，请检查seriesName参数", groups = {add.class, edit.class})
    private String seriesName;

    /**
     * 车型名称
     */
    @NotBlank(message = "车型名称不能为空，请检查modelName参数", groups = {add.class, edit.class})
    private String modelName;

    /**
     * 由于车辆型号是从车行168（大搜车旗下）爬取，所以用此ID与其对应。
     */
    @NotBlank(message = "由于车辆型号是从车行168（大搜车旗下）爬取，所以用此ID与其对应。不能为空，请检查dscModelId参数", groups = {add.class, edit.class})
    private String dscModelId;

    /**
     * 车型配置信息
     */
    @NotBlank(message = "车型配置信息不能为空，请检查modelJson参数", groups = {add.class, edit.class})
    private String modelJson;

    /**
     * 车型编码
     */
    @NotBlank(message = "车型编码不能为空，请检查modelCode参数", groups = {add.class, edit.class})
    private String modelCode;

    /**
     * 外观颜色
     */
    @NotBlank(message = "外观颜色不能为空，请检查appearanceColor参数", groups = {add.class, edit.class})
    private String appearanceColor;

    /**
     * 内饰颜色
     */
    @NotBlank(message = "内饰颜色不能为空，请检查interiorColor参数", groups = {add.class, edit.class})
    private String interiorColor;

    /**
     * 销售类型
     */
    @NotBlank(message = "销售类型不能为空，请检查saleType参数", groups = {add.class, edit.class})
    private String saleType;

    /**
     * 年款
     */
    @NotBlank(message = "年款不能为空，请检查modelYear参数", groups = {add.class, edit.class})
    private String modelYear;

    /**
     * 排量(车型首页)
     */
    @NotBlank(message = "排量(车型首页)不能为空，请检查exhaust参数", groups = {add.class, edit.class})
    private String exhaust;

    /**
     * 变速箱（车型首页）
     */
    @NotBlank(message = "变速箱（车型首页）不能为空，请检查shiftBox参数", groups = {add.class, edit.class})
    private String shiftBox;

    /**
     * 上市日期
     */
    @NotBlank(message = "上市日期不能为空，请检查launchDate参数", groups = {add.class, edit.class})
    private String launchDate;

    /**
     * 车身结构
     */
    @NotBlank(message = "车身结构不能为空，请检查bodyTypeName参数", groups = {add.class, edit.class})
    private String bodyTypeName;

    /**
     * 发动机类型
     */
    @NotBlank(message = "发动机类型不能为空，请检查engineTypeName参数", groups = {add.class, edit.class})
    private String engineTypeName;

    /**
     * 级别（车型首页）
     */
    @NotBlank(message = "级别（车型首页）不能为空，请检查carGrade参数", groups = {add.class, edit.class})
    private String carGrade;

    /**
     * 汽车类型
     */
    @NotBlank(message = "汽车类型不能为空，请检查carType参数", groups = {add.class, edit.class})
    private String carType;

    /**
     * 子品牌id
     */
    @NotBlank(message = "子品牌id不能为空，请检查subBrandNo参数", groups = {add.class, edit.class})
    private String subBrandNo;

    /**
     * 子品牌名称
     */
    @NotBlank(message = "子品牌名称不能为空，请检查subBrandName参数", groups = {add.class, edit.class})
    private String subBrandName;

    /**
     * 指导价
     */
    @NotBlank(message = "指导价不能为空，请检查guidingPrice参数", groups = {add.class, edit.class})
    private String guidingPrice;

    /**
     * 环保标准（车型首页）
     */
    @NotBlank(message = "环保标准（车型首页）不能为空，请检查emissionStandard参数", groups = {add.class, edit.class})
    private String emissionStandard;

    /**
     * 综合油耗(工信部)（车型首页）
     */
    @NotBlank(message = "综合油耗(工信部)（车型首页）不能为空，请检查oilConsumption参数", groups = {add.class, edit.class})
    private String oilConsumption;

    /**
     * 最大功率（车型首页）
     */
    @NotBlank(message = "最大功率（车型首页）不能为空，请检查maxPower参数", groups = {add.class, edit.class})
    private String maxPower;

    /**
     * 最大扭矩（车型首页）
     */
    @NotBlank(message = "最大扭矩（车型首页）不能为空，请检查maxTorsion参数", groups = {add.class, edit.class})
    private String maxTorsion;

    /**
     * 能源类型（车型首页）
     */
    @NotBlank(message = "能源类型（车型首页）不能为空，请检查energyType参数", groups = {add.class, edit.class})
    private String energyType;

    /**
     * 快充时间（车型首页）
     */
    @NotBlank(message = "快充时间（车型首页）不能为空，请检查fastChargingTime参数", groups = {add.class, edit.class})
    private String fastChargingTime;

    /**
     * 慢充时间（车型首页）
     */
    @NotBlank(message = "慢充时间（车型首页）不能为空，请检查slowChargingTime参数", groups = {add.class, edit.class})
    private String slowChargingTime;

    /**
     * 续航里程（车型首页）
     */
    @NotBlank(message = "续航里程（车型首页）不能为空，请检查enduranceMileage参数", groups = {add.class, edit.class})
    private String enduranceMileage;

    /**
     * 0,未删除
            1,已删除
     */
    @NotNull(message = "0,未删除1,已删除不能为空，请检查deleteFlag参数", groups = {add.class, edit.class})
    private Integer deleteFlag;

    /**
     *
     */
    @NotBlank(message = "不能为空，请检查remark参数", groups = {add.class, edit.class})
    private String remark;

}
