package yh.platform.modular.carmodel.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.afterturn.easypoi.excel.annotation.Excel;
import yh.platform.core.pojo.base.entity.BaseEntity;

/**
 * 车型表
 *
 * @author gyp
 * @date 2023-07-24 18:40:10
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("car_model")
public class CarModel extends BaseEntity {

    /**
     *
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 汽车之家车型id
     */
    @Excel(name = "汽车之家车型id")
    private Long qczjModelId;

    /**
     * 品牌id
     */
    @Excel(name = "品牌id")
    private Long brandId;

    /**
     * 品牌名称
     */
    @Excel(name = "品牌名称")
    private String brandName;

    /**
     * 车系id
     */
    @Excel(name = "车系id")
    private Long seriesId;

    /**
     * 车系名称
     */
    @Excel(name = "车系名称")
    private String seriesName;

    /**
     * 车型名称
     */
    @Excel(name = "车型名称")
    private String modelName;

    /**
     * 由于车辆型号是从车行168（大搜车旗下）爬取，所以用此ID与其对应。
     */
    @Excel(name = "由于车辆型号是从车行168（大搜车旗下）爬取，所以用此ID与其对应。")
    private String dscModelId;

    /**
     * 车型配置信息
     */
    @Excel(name = "车型配置信息")
    private String modelJson;

    /**
     * 车型编码
     */
    @Excel(name = "车型编码")
    private String modelCode;

    /**
     * 外观颜色
     */
    @Excel(name = "外观颜色")
    private String appearanceColor;

    /**
     * 内饰颜色
     */
    @Excel(name = "内饰颜色")
    private String interiorColor;

    /**
     * 销售类型
     */
    @Excel(name = "销售类型")
    private String saleType;

    /**
     * 年款
     */
    @Excel(name = "年款")
    private String modelYear;

    /**
     * 排量(车型首页)
     */
    @Excel(name = "排量(车型首页)")
    private String exhaust;

    /**
     * 变速箱（车型首页）
     */
    @Excel(name = "变速箱（车型首页）")
    private String shiftBox;

    /**
     * 上市日期
     */
    @Excel(name = "上市日期")
    private String launchDate;

    /**
     * 车身结构
     */
    @Excel(name = "车身结构")
    private String bodyTypeName;

    /**
     * 发动机类型
     */
    @Excel(name = "发动机类型")
    private String engineTypeName;

    /**
     * 级别（车型首页）
     */
    @Excel(name = "级别（车型首页）")
    private String carGrade;

    /**
     * 汽车类型
     */
    @Excel(name = "汽车类型")
    private String carType;

    /**
     * 子品牌id
     */
    @Excel(name = "子品牌id")
    private String subBrandNo;

    /**
     * 子品牌名称
     */
    @Excel(name = "子品牌名称")
    private String subBrandName;

    /**
     * 指导价
     */
    @Excel(name = "指导价")
    private String guidingPrice;

    /**
     * 环保标准（车型首页）
     */
    @Excel(name = "环保标准（车型首页）")
    private String emissionStandard;

    /**
     * 综合油耗(工信部)（车型首页）
     */
    @Excel(name = "综合油耗(工信部)（车型首页）")
    private String oilConsumption;

    /**
     * 最大功率（车型首页）
     */
    @Excel(name = "最大功率（车型首页）")
    private String maxPower;

    /**
     * 最大扭矩（车型首页）
     */
    @Excel(name = "最大扭矩（车型首页）")
    private String maxTorsion;

    /**
     * 能源类型（车型首页）
     */
    @Excel(name = "能源类型（车型首页）")
    private String energyType;

    /**
     * 快充时间（车型首页）
     */
    @Excel(name = "快充时间（车型首页）")
    private String fastChargingTime;

    /**
     * 慢充时间（车型首页）
     */
    @Excel(name = "慢充时间（车型首页）")
    private String slowChargingTime;

    /**
     * 续航里程（车型首页）
     */
    @Excel(name = "续航里程（车型首页）")
    private String enduranceMileage;

    /**
     * 0,未删除
            1,已删除
     */
    @Excel(name = "0,未删除1,已删除")
    private Integer deleteFlag;

    /**
     *
     */
    @Excel(name = "")
    private String remark;

}
