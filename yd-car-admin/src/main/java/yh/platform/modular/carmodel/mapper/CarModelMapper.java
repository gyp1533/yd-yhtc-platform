package yh.platform.modular.carmodel.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import yh.platform.modular.carmodel.entity.CarModel;

/**
 * 车型表
 *
 * @author gyp
 * @date 2023-07-24 18:40:10
 */
public interface CarModelMapper extends BaseMapper<CarModel> {
}
