package yh.platform.modular.carmodel.service;

import com.baomidou.mybatisplus.extension.service.IService;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.modular.carmodel.entity.CarModel;
import yh.platform.modular.carmodel.param.CarModelParam;
import java.util.List;

/**
 * 车型表service接口
 *
 * @author gyp
 * @date 2023-07-24 18:40:10
 */
public interface CarModelService extends IService<CarModel> {

    /**
     * 查询车型表
     *
     * @author gyp
     * @date 2023-07-24 18:40:10
     */
    PageResult<CarModel> page(CarModelParam carModelParam);

    /**
     * 车型表列表
     *
     * @author gyp
     * @date 2023-07-24 18:40:10
     */
    List<CarModel> list(CarModelParam carModelParam);

    /**
     * 添加车型表
     *
     * @author gyp
     * @date 2023-07-24 18:40:10
     */
    void add(CarModelParam carModelParam);

    /**
     * 删除车型表
     *
     * @author gyp
     * @date 2023-07-24 18:40:10
     */
    void delete(List<CarModelParam> carModelParamList);

    /**
     * 编辑车型表
     *
     * @author gyp
     * @date 2023-07-24 18:40:10
     */
    void edit(CarModelParam carModelParam);

    /**
     * 查看车型表
     *
     * @author gyp
     * @date 2023-07-24 18:40:10
     */
     CarModel detail(CarModelParam carModelParam);

    /**
     * 导出车型表
     *
     * @author gyp
     * @date 2023-07-24 18:40:10
     */
     void export(CarModelParam carModelParam);

}
