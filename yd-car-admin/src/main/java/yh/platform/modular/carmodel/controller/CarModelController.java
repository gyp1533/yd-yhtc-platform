package yh.platform.modular.carmodel.controller;

import yh.platform.core.annotion.BusinessLog;
import yh.platform.core.pojo.response.ResponseData;
import yh.platform.core.pojo.response.SuccessResponseData;
import yh.platform.core.annotion.Permission;
import yh.platform.core.enums.LogAnnotionOpTypeEnum;
import yh.platform.core.pojo.response.ResponseData;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.core.pojo.response.SuccessResponseData;
import yh.platform.modular.carmodel.entity.CarModel;
import yh.platform.modular.carmodel.param.CarModelParam;
import yh.platform.modular.carmodel.service.CarModelService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 车型表控制器
 *
 * @author gyp
 * @date 2023-07-24 18:40:10
 */
@RestController
public class CarModelController {

    @Resource
    private CarModelService carModelService;

//    /**
//     * 查询车型表
//     *
//     * @author gyp
//     * @date 2023-07-24 18:40:10
//     */
//    @PostMapping("/carModel/page")
//    public ResponseData<PageResult<CarModel>> page(CarModelParam carModelParam) {
//        return SuccessResponseData.success(carModelService.page(carModelParam));
//    }
//
//    /**
//     * 添加车型表
//     *
//     * @author gyp
//     * @date 2023-07-24 18:40:10
//     */
//    @PostMapping("/carModel/add")
//    public ResponseData add(@RequestBody @Validated(CarModelParam.add.class) CarModelParam carModelParam) {
//        carModelService.add(carModelParam);
//		return SuccessResponseData.success();
//    }
//
//    /**
//     * 删除车型表，可批量删除
//     *
//     * @author gyp
//     * @date 2023-07-24 18:40:10
//     */
//    @PostMapping("/carModel/delete")
//    public ResponseData delete(@RequestBody @Validated(CarModelParam.delete.class) List<CarModelParam> carModelParamList) {
//        carModelService.delete(carModelParamList);
//		return SuccessResponseData.success();
//    }
//
//    /**
//     * 编辑车型表
//     *
//     * @author gyp
//     * @date 2023-07-24 18:40:10
//     */
//    @PostMapping("/carModel/edit")
//    public ResponseData edit(@RequestBody @Validated(CarModelParam.edit.class) CarModelParam carModelParam) {
//        carModelService.edit(carModelParam);
//		return SuccessResponseData.success();
//    }
//
//    /**
//     * 查看车型表
//     *
//     * @author gyp
//     * @date 2023-07-24 18:40:10
//     */
//    @PostMapping("/carModel/detail")
//    public ResponseData<CarModel> detail(@RequestBody @Validated(CarModelParam.detail.class) CarModelParam carModelParam) {
//
//        return SuccessResponseData.success(carModelService.detail(carModelParam));
//    }

    /**
     * 车型表列表
     *
     * @author gyp
     * @date 2023-07-24 18:40:10
     */
    @GetMapping("/car/carModel/list")
    public ResponseData<List<CarModel>> list(CarModelParam carModelParam) {
        return SuccessResponseData.success(carModelService.list(carModelParam));
    }

}
