package yh.platform.modular.carmodel.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import yh.platform.core.exception.ServiceException;
import yh.platform.core.factory.PageFactory;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.core.util.PoiUtil;
import yh.platform.modular.carmodel.entity.CarModel;
import yh.platform.modular.carmodel.enums.CarModelExceptionEnum;
import yh.platform.modular.carmodel.mapper.CarModelMapper;
import yh.platform.modular.carmodel.param.CarModelParam;
import yh.platform.modular.carmodel.service.CarModelService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 车型表service接口实现类
 *
 * @author gyp
 * @date 2023-07-24 18:40:10
 */
@Service
public class CarModelServiceImpl extends ServiceImpl<CarModelMapper, CarModel> implements CarModelService {

    @Override
    public PageResult<CarModel> page(CarModelParam carModelParam) {
        QueryWrapper<CarModel> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(carModelParam)) {

            // 根据汽车之家车型id 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getQczjModelId())) {
                queryWrapper.lambda().eq(CarModel::getQczjModelId, carModelParam.getQczjModelId());
            }
            // 根据品牌id 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getBrandId())) {
                queryWrapper.lambda().eq(CarModel::getBrandId, carModelParam.getBrandId());
            }
            // 根据品牌名称 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getBrandName())) {
                queryWrapper.lambda().eq(CarModel::getBrandName, carModelParam.getBrandName());
            }
            // 根据车系id 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getSeriesId())) {
                queryWrapper.lambda().eq(CarModel::getSeriesId, carModelParam.getSeriesId());
            }
            // 根据车系名称 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getSeriesName())) {
                queryWrapper.lambda().eq(CarModel::getSeriesName, carModelParam.getSeriesName());
            }
            // 根据车型名称 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getModelName())) {
                queryWrapper.lambda().eq(CarModel::getModelName, carModelParam.getModelName());
            }
            // 根据由于车辆型号是从车行168（大搜车旗下）爬取，所以用此ID与其对应。 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getDscModelId())) {
                queryWrapper.lambda().eq(CarModel::getDscModelId, carModelParam.getDscModelId());
            }
            // 根据车型配置信息 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getModelJson())) {
                queryWrapper.lambda().eq(CarModel::getModelJson, carModelParam.getModelJson());
            }
            // 根据车型编码 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getModelCode())) {
                queryWrapper.lambda().eq(CarModel::getModelCode, carModelParam.getModelCode());
            }
            // 根据外观颜色 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getAppearanceColor())) {
                queryWrapper.lambda().eq(CarModel::getAppearanceColor, carModelParam.getAppearanceColor());
            }
            // 根据内饰颜色 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getInteriorColor())) {
                queryWrapper.lambda().eq(CarModel::getInteriorColor, carModelParam.getInteriorColor());
            }
            // 根据销售类型 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getSaleType())) {
                queryWrapper.lambda().eq(CarModel::getSaleType, carModelParam.getSaleType());
            }
            // 根据年款 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getModelYear())) {
                queryWrapper.lambda().eq(CarModel::getModelYear, carModelParam.getModelYear());
            }
            // 根据排量(车型首页) 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getExhaust())) {
                queryWrapper.lambda().eq(CarModel::getExhaust, carModelParam.getExhaust());
            }
            // 根据变速箱（车型首页） 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getShiftBox())) {
                queryWrapper.lambda().eq(CarModel::getShiftBox, carModelParam.getShiftBox());
            }
            // 根据上市日期 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getLaunchDate())) {
                queryWrapper.lambda().eq(CarModel::getLaunchDate, carModelParam.getLaunchDate());
            }
            // 根据车身结构 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getBodyTypeName())) {
                queryWrapper.lambda().eq(CarModel::getBodyTypeName, carModelParam.getBodyTypeName());
            }
            // 根据发动机类型 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getEngineTypeName())) {
                queryWrapper.lambda().eq(CarModel::getEngineTypeName, carModelParam.getEngineTypeName());
            }
            // 根据级别（车型首页） 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getCarGrade())) {
                queryWrapper.lambda().eq(CarModel::getCarGrade, carModelParam.getCarGrade());
            }
            // 根据汽车类型 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getCarType())) {
                queryWrapper.lambda().eq(CarModel::getCarType, carModelParam.getCarType());
            }
            // 根据子品牌id 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getSubBrandNo())) {
                queryWrapper.lambda().eq(CarModel::getSubBrandNo, carModelParam.getSubBrandNo());
            }
            // 根据子品牌名称 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getSubBrandName())) {
                queryWrapper.lambda().eq(CarModel::getSubBrandName, carModelParam.getSubBrandName());
            }
            // 根据指导价 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getGuidingPrice())) {
                queryWrapper.lambda().eq(CarModel::getGuidingPrice, carModelParam.getGuidingPrice());
            }
            // 根据环保标准（车型首页） 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getEmissionStandard())) {
                queryWrapper.lambda().eq(CarModel::getEmissionStandard, carModelParam.getEmissionStandard());
            }
            // 根据综合油耗(工信部)（车型首页） 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getOilConsumption())) {
                queryWrapper.lambda().eq(CarModel::getOilConsumption, carModelParam.getOilConsumption());
            }
            // 根据最大功率（车型首页） 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getMaxPower())) {
                queryWrapper.lambda().eq(CarModel::getMaxPower, carModelParam.getMaxPower());
            }
            // 根据最大扭矩（车型首页） 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getMaxTorsion())) {
                queryWrapper.lambda().eq(CarModel::getMaxTorsion, carModelParam.getMaxTorsion());
            }
            // 根据能源类型（车型首页） 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getEnergyType())) {
                queryWrapper.lambda().eq(CarModel::getEnergyType, carModelParam.getEnergyType());
            }
            // 根据快充时间（车型首页） 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getFastChargingTime())) {
                queryWrapper.lambda().eq(CarModel::getFastChargingTime, carModelParam.getFastChargingTime());
            }
            // 根据慢充时间（车型首页） 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getSlowChargingTime())) {
                queryWrapper.lambda().eq(CarModel::getSlowChargingTime, carModelParam.getSlowChargingTime());
            }
            // 根据续航里程（车型首页） 查询
            if (ObjectUtil.isNotEmpty(carModelParam.getEnduranceMileage())) {
                queryWrapper.lambda().eq(CarModel::getEnduranceMileage, carModelParam.getEnduranceMileage());
            }
        }
        return new PageResult<>(this.page(PageFactory.defaultPage(), queryWrapper));
    }

    @Override
    public List<CarModel> list(CarModelParam carModelParam) {
        QueryWrapper<CarModel> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(carModelParam)) {
            if (ObjectUtil.isNotEmpty(carModelParam.getId())) {
                queryWrapper.lambda().eq(CarModel::getSeriesId, carModelParam.getId());
            }
        }
        return this.list(queryWrapper);
    }

    @Override
    public void add(CarModelParam carModelParam) {
        CarModel carModel = new CarModel();
        BeanUtil.copyProperties(carModelParam, carModel);
        this.save(carModel);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<CarModelParam> carModelParamList) {
        carModelParamList.forEach(carModelParam -> {
            this.removeById(carModelParam.getId());
        });
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(CarModelParam carModelParam) {
        CarModel carModel = this.queryCarModel(carModelParam);
        BeanUtil.copyProperties(carModelParam, carModel);
        this.updateById(carModel);
    }

    @Override
    public CarModel detail(CarModelParam carModelParam) {
        return this.queryCarModel(carModelParam);
    }

    /**
     * 获取车型表
     *
     * @author gyp
     * @date 2023-07-24 18:40:10
     */
    private CarModel queryCarModel(CarModelParam carModelParam) {
        CarModel carModel = this.getById(carModelParam.getId());
        if (ObjectUtil.isNull(carModel)) {
            throw new ServiceException(CarModelExceptionEnum.NOT_EXIST);
        }
        return carModel;
    }

    @Override
    public void export(CarModelParam carModelParam) {
        List<CarModel> list = this.list(carModelParam);
        PoiUtil.exportExcelWithStream("CarModel.xls", CarModel.class, list);
    }

}
