package yh.platform.modular.carseries.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.afterturn.easypoi.excel.annotation.Excel;
import yh.platform.core.pojo.base.entity.BaseEntity;

/**
 * 车系表
 *
 * @author gyp
 * @date 2023-07-24 19:18:45
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("car_series")
public class CarSeries extends BaseEntity {

    /**
     *
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 汽车之家源网站车系id
     */
    @Excel(name = "汽车之家源网站车系id")
    private Long qczjSeriesId;

    /**
     * 品牌id
     */
    @Excel(name = "品牌id")
    private Long brandId;

    /**
     * 子品牌id
     */
    @Excel(name = "子品牌id")
    private Long subBrandId;

    /**
     * 车系类型（对应中台车身类型）
     */
    @Excel(name = "车系类型（对应中台车身类型）")
    private String seriesType;

    /**
     * 车系名称
     */
    @Excel(name = "车系名称")
    private String seriesName;

    /**
     * 车系编号
     */
    @Excel(name = "车系编号")
    private String seriesNo;

    /**
     * 子品牌名称
     */
    @Excel(name = "子品牌名称")
    private String subBrand;

    /**
     * 0,否
            1,是
     */
    @Excel(name = "0,否1,是")
    private Integer isParallelImport;

    /**
     * 0,不展示
            1,展示
     */
    @Excel(name = "0,不展示1,展示")
    private Integer isShow;

    /**
     *
     */
    @Excel(name = "")
    private String seriesUrl;

    /**
     *
     */
    @Excel(name = "")
    private String saleType;

    /**
     * 0,未删除
            1,已删除
     */
    @Excel(name = "0,未删除1,已删除")
    private Integer deleteFlag;

    /**
     *
     */
    @Excel(name = "")
    private String remark;

}
