package yh.platform.modular.carseries.param;

import yh.platform.core.pojo.base.param.BaseParam;
import lombok.Data;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

/**
* 车系表参数类
 *
 * @author gyp
 * @date 2023-07-24 19:18:45
*/
@Data
public class CarSeriesParam extends BaseParam {

    /**
     *
     */
    @NotNull(message = "不能为空，请检查id参数", groups = {edit.class, delete.class, detail.class})
    private Long id;

    /**
     * 汽车之家源网站车系id
     */
    @NotNull(message = "汽车之家源网站车系id不能为空，请检查qczjSeriesId参数", groups = {add.class, edit.class})
    private Long qczjSeriesId;

    /**
     * 品牌id
     */
    @NotNull(message = "品牌id不能为空，请检查brandId参数", groups = {add.class, edit.class})
    private Long brandId;

    /**
     * 子品牌id
     */
    @NotNull(message = "子品牌id不能为空，请检查subBrandId参数", groups = {add.class, edit.class})
    private Long subBrandId;

    /**
     * 车系类型（对应中台车身类型）
     */
    @NotBlank(message = "车系类型（对应中台车身类型）不能为空，请检查seriesType参数", groups = {add.class, edit.class})
    private String seriesType;

    /**
     * 车系名称
     */
    @NotBlank(message = "车系名称不能为空，请检查seriesName参数", groups = {add.class, edit.class})
    private String seriesName;

    /**
     * 车系编号
     */
    @NotBlank(message = "车系编号不能为空，请检查seriesNo参数", groups = {add.class, edit.class})
    private String seriesNo;

    /**
     * 子品牌名称
     */
    @NotBlank(message = "子品牌名称不能为空，请检查subBrand参数", groups = {add.class, edit.class})
    private String subBrand;

    /**
     * 0,否
            1,是
     */
    @NotNull(message = "0,否1,是不能为空，请检查isParallelImport参数", groups = {add.class, edit.class})
    private Integer isParallelImport;

    /**
     * 0,不展示
            1,展示
     */
    @NotNull(message = "0,不展示1,展示不能为空，请检查isShow参数", groups = {add.class, edit.class})
    private Integer isShow;

    /**
     *
     */
    @NotBlank(message = "不能为空，请检查seriesUrl参数", groups = {add.class, edit.class})
    private String seriesUrl;

    /**
     *
     */
    @NotBlank(message = "不能为空，请检查saleType参数", groups = {add.class, edit.class})
    private String saleType;

    /**
     * 0,未删除
            1,已删除
     */
    @NotNull(message = "0,未删除1,已删除不能为空，请检查deleteFlag参数", groups = {add.class, edit.class})
    private Integer deleteFlag;

    /**
     *
     */
    @NotNull(message = "不能为空，请检查createBy参数", groups = {add.class, edit.class})
    private Long createBy;

    /**
     *
     */
    @NotNull(message = "不能为空，请检查updateBy参数", groups = {add.class, edit.class})
    private Long updateBy;

    /**
     *
     */
    @NotBlank(message = "不能为空，请检查remark参数", groups = {add.class, edit.class})
    private String remark;

}
