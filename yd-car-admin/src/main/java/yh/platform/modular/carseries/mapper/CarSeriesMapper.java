package yh.platform.modular.carseries.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import yh.platform.modular.carseries.entity.CarSeries;

/**
 * 车系表
 *
 * @author gyp
 * @date 2023-07-24 19:18:45
 */
public interface CarSeriesMapper extends BaseMapper<CarSeries> {
}
