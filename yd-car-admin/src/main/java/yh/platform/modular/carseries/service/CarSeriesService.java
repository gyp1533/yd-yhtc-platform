package yh.platform.modular.carseries.service;

import com.baomidou.mybatisplus.extension.service.IService;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.modular.carseries.entity.CarSeries;
import yh.platform.modular.carseries.param.CarSeriesParam;
import java.util.List;

/**
 * 车系表service接口
 *
 * @author gyp
 * @date 2023-07-24 19:18:45
 */
public interface CarSeriesService extends IService<CarSeries> {

    /**
     * 查询车系表
     *
     * @author gyp
     * @date 2023-07-24 19:18:45
     */
    PageResult<CarSeries> page(CarSeriesParam carSeriesParam);

    /**
     * 车系表列表
     *
     * @author gyp
     * @date 2023-07-24 19:18:45
     */
    List<CarSeries> list(CarSeriesParam carSeriesParam);

    /**
     * 添加车系表
     *
     * @author gyp
     * @date 2023-07-24 19:18:45
     */
    void add(CarSeriesParam carSeriesParam);

    /**
     * 删除车系表
     *
     * @author gyp
     * @date 2023-07-24 19:18:45
     */
    void delete(List<CarSeriesParam> carSeriesParamList);

    /**
     * 编辑车系表
     *
     * @author gyp
     * @date 2023-07-24 19:18:45
     */
    void edit(CarSeriesParam carSeriesParam);

    /**
     * 查看车系表
     *
     * @author gyp
     * @date 2023-07-24 19:18:45
     */
     CarSeries detail(CarSeriesParam carSeriesParam);

    /**
     * 导出车系表
     *
     * @author gyp
     * @date 2023-07-24 19:18:45
     */
     void export(CarSeriesParam carSeriesParam);

}
