package yh.platform.modular.carseries.controller;

import yh.platform.core.annotion.BusinessLog;
import yh.platform.core.annotion.Permission;
import yh.platform.core.enums.LogAnnotionOpTypeEnum;
import yh.platform.core.pojo.response.ResponseData;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.core.pojo.response.SuccessResponseData;
import yh.platform.modular.carseries.entity.CarSeries;
import yh.platform.modular.carseries.param.CarSeriesParam;
import yh.platform.modular.carseries.service.CarSeriesService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 车系表控制器
 *
 * @author gyp
 * @date 2023-07-24 19:18:45
 */
@RestController
public class CarSeriesController {

//    @Resource
//    private CarSeriesService carSeriesService;
//
//    /**
//     * 查询车系表
//     *
//     * @author gyp
//     * @date 2023-07-24 19:18:45
//     */
//    @PostMapping("/carSeries/page")
//    public ResponseData<PageResult<CarSeries>> page(CarSeriesParam carSeriesParam) {
//        return SuccessResponseData.success(carSeriesService.page(carSeriesParam));
//    }
//
//    /**
//     * 添加车系表
//     *
//     * @author gyp
//     * @date 2023-07-24 19:18:45
//     */
//    @PostMapping("/carSeries/add")
//    public ResponseData add(@RequestBody @Validated(CarSeriesParam.add.class) CarSeriesParam carSeriesParam) {
//        carSeriesService.add(carSeriesParam);
//		return SuccessResponseData.success();
//    }
//
//    /**
//     * 删除车系表，可批量删除
//     *
//     * @author gyp
//     * @date 2023-07-24 19:18:45
//     */
//    @PostMapping("/carSeries/delete")
//    public ResponseData delete(@RequestBody @Validated(CarSeriesParam.delete.class) List<CarSeriesParam> carSeriesParamList) {
//        carSeriesService.delete(carSeriesParamList);
//		return SuccessResponseData.success();
//    }
//
//    /**
//     * 编辑车系表
//     *
//     * @author gyp
//     * @date 2023-07-24 19:18:45
//     */
//    @PostMapping("/carSeries/edit")
//    public ResponseData edit(@RequestBody @Validated(CarSeriesParam.edit.class) CarSeriesParam carSeriesParam) {
//        carSeriesService.edit(carSeriesParam);
//		return SuccessResponseData.success();
//    }
//
//    /**
//     * 查看车系表
//     *
//     * @author gyp
//     * @date 2023-07-24 19:18:45
//     */
//    @PostMapping("/carSeries/detail")
//    public ResponseData<CarSeries> detail(@RequestBody @Validated(CarSeriesParam.detail.class) CarSeriesParam carSeriesParam) {
//
//        return SuccessResponseData.success(carSeriesService.detail(carSeriesParam));
//    }
//
//    /**
//     * 车系表列表
//     *
//     * @author gyp
//     * @date 2023-07-24 19:18:45
//     */
//    @PostMapping("/carSeries/list")
//    public ResponseData<List<CarSeries>> list(CarSeriesParam carSeriesParam) {
//        return SuccessResponseData.success(carSeriesService.list(carSeriesParam));
//    }
//
//    /**
//     * 导出系统用户
//     *
//     * @author gyp
//     * @date 2023-07-24 19:18:45
//     */
//    @PostMapping("/carSeries/export")
//    public void export(CarSeriesParam carSeriesParam) {
//        carSeriesService.export(carSeriesParam);
//    }

}
