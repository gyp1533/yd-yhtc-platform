package yh.platform.modular.carseries.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import yh.platform.core.exception.ServiceException;
import yh.platform.core.factory.PageFactory;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.core.util.PoiUtil;
import yh.platform.modular.carseries.entity.CarSeries;
import yh.platform.modular.carseries.enums.CarSeriesExceptionEnum;
import yh.platform.modular.carseries.mapper.CarSeriesMapper;
import yh.platform.modular.carseries.param.CarSeriesParam;
import yh.platform.modular.carseries.service.CarSeriesService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 车系表service接口实现类
 *
 * @author gyp
 * @date 2023-07-24 19:18:45
 */
@Service
public class CarSeriesServiceImpl extends ServiceImpl<CarSeriesMapper, CarSeries> implements CarSeriesService {

    @Override
    public PageResult<CarSeries> page(CarSeriesParam carSeriesParam) {
        QueryWrapper<CarSeries> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(carSeriesParam)) {

            // 根据汽车之家源网站车系id 查询
            if (ObjectUtil.isNotEmpty(carSeriesParam.getQczjSeriesId())) {
                queryWrapper.lambda().eq(CarSeries::getQczjSeriesId, carSeriesParam.getQczjSeriesId());
            }
            // 根据品牌id 查询
            if (ObjectUtil.isNotEmpty(carSeriesParam.getBrandId())) {
                queryWrapper.lambda().eq(CarSeries::getBrandId, carSeriesParam.getBrandId());
            }
            // 根据子品牌id 查询
            if (ObjectUtil.isNotEmpty(carSeriesParam.getSubBrandId())) {
                queryWrapper.lambda().eq(CarSeries::getSubBrandId, carSeriesParam.getSubBrandId());
            }
            // 根据车系类型（对应中台车身类型） 查询
            if (ObjectUtil.isNotEmpty(carSeriesParam.getSeriesType())) {
                queryWrapper.lambda().eq(CarSeries::getSeriesType, carSeriesParam.getSeriesType());
            }
            // 根据车系名称 查询
            if (ObjectUtil.isNotEmpty(carSeriesParam.getSeriesName())) {
                queryWrapper.lambda().eq(CarSeries::getSeriesName, carSeriesParam.getSeriesName());
            }
            // 根据车系编号 查询
            if (ObjectUtil.isNotEmpty(carSeriesParam.getSeriesNo())) {
                queryWrapper.lambda().eq(CarSeries::getSeriesNo, carSeriesParam.getSeriesNo());
            }
            // 根据子品牌名称 查询
            if (ObjectUtil.isNotEmpty(carSeriesParam.getSubBrand())) {
                queryWrapper.lambda().eq(CarSeries::getSubBrand, carSeriesParam.getSubBrand());
            }
            // 根据0,否 1,是 查询
            if (ObjectUtil.isNotEmpty(carSeriesParam.getIsParallelImport())) {
                queryWrapper.lambda().eq(CarSeries::getIsParallelImport, carSeriesParam.getIsParallelImport());
            }
            // 根据0,不展示1,展示 查询
            if (ObjectUtil.isNotEmpty(carSeriesParam.getIsShow())) {
                queryWrapper.lambda().eq(CarSeries::getIsShow, carSeriesParam.getIsShow());
            }
            // 根据 查询
            if (ObjectUtil.isNotEmpty(carSeriesParam.getSeriesUrl())) {
                queryWrapper.lambda().eq(CarSeries::getSeriesUrl, carSeriesParam.getSeriesUrl());
            }
            // 根据 查询
            if (ObjectUtil.isNotEmpty(carSeriesParam.getSaleType())) {
                queryWrapper.lambda().eq(CarSeries::getSaleType, carSeriesParam.getSaleType());
            }
            // 根据0,未删除1,已删除 查询
            if (ObjectUtil.isNotEmpty(carSeriesParam.getDeleteFlag())) {
                queryWrapper.lambda().eq(CarSeries::getDeleteFlag, carSeriesParam.getDeleteFlag());
            }
            // 根据 查询
            if (ObjectUtil.isNotEmpty(carSeriesParam.getRemark())) {
                queryWrapper.lambda().eq(CarSeries::getRemark, carSeriesParam.getRemark());
            }
        }
        return new PageResult<>(this.page(PageFactory.defaultPage(), queryWrapper));
    }

    @Override
    public List<CarSeries> list(CarSeriesParam carSeriesParam) {
        QueryWrapper<CarSeries> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(carSeriesParam)) {
            // 根据子品牌id 查询
            if (ObjectUtil.isNotEmpty(carSeriesParam.getSubBrandId())) {
                queryWrapper.lambda().eq(CarSeries::getSubBrandId, carSeriesParam.getSubBrandId());
            }
        }
        return this.list(queryWrapper);
    }

    @Override
    public void add(CarSeriesParam carSeriesParam) {
        CarSeries carSeries = new CarSeries();
        BeanUtil.copyProperties(carSeriesParam, carSeries);
        this.save(carSeries);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<CarSeriesParam> carSeriesParamList) {
        carSeriesParamList.forEach(carSeriesParam -> {
            this.removeById(carSeriesParam.getId());
        });
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(CarSeriesParam carSeriesParam) {
        CarSeries carSeries = this.queryCarSeries(carSeriesParam);
        BeanUtil.copyProperties(carSeriesParam, carSeries);
        this.updateById(carSeries);
    }

    @Override
    public CarSeries detail(CarSeriesParam carSeriesParam) {
        return this.queryCarSeries(carSeriesParam);
    }

    /**
     * 获取车系表
     *
     * @author gyp
     * @date 2023-07-24 19:18:45
     */
    private CarSeries queryCarSeries(CarSeriesParam carSeriesParam) {
        CarSeries carSeries = this.getById(carSeriesParam.getId());
        if (ObjectUtil.isNull(carSeries)) {
            throw new ServiceException(CarSeriesExceptionEnum.NOT_EXIST);
        }
        return carSeries;
    }

    @Override
    public void export(CarSeriesParam carSeriesParam) {
        List<CarSeries> list = this.list(carSeriesParam);
        PoiUtil.exportExcelWithStream("CarSeries.xls", CarSeries.class, list);
    }

}
