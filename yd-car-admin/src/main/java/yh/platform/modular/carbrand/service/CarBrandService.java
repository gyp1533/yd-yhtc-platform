package yh.platform.modular.carbrand.service;

import com.baomidou.mybatisplus.extension.service.IService;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.modular.carbrand.entity.CarBrand;
import yh.platform.modular.carbrand.param.CarBrandParam;
import java.util.List;

/**
 * 品牌表service接口
 *
 * @author gyp
 * @date 2023-07-24 18:49:44
 */
public interface CarBrandService extends IService<CarBrand> {

    /**
     * 查询品牌表
     *
     * @author gyp
     * @date 2023-07-24 18:49:44
     */
    PageResult<CarBrand> page(CarBrandParam carBrandParam);

    /**
     * 品牌表列表
     *
     * @author gyp
     * @date 2023-07-24 18:49:44
     */
    List<CarBrand> list(CarBrandParam carBrandParam);

    /**
     * 添加品牌表
     *
     * @author gyp
     * @date 2023-07-24 18:49:44
     */
    void add(CarBrandParam carBrandParam);

    /**
     * 删除品牌表
     *
     * @author gyp
     * @date 2023-07-24 18:49:44
     */
    void delete(List<CarBrandParam> carBrandParamList);

    /**
     * 编辑品牌表
     *
     * @author gyp
     * @date 2023-07-24 18:49:44
     */
    void edit(CarBrandParam carBrandParam);

    /**
     * 查看品牌表
     *
     * @author gyp
     * @date 2023-07-24 18:49:44
     */
     CarBrand detail(CarBrandParam carBrandParam);

    /**
     * 导出品牌表
     *
     * @author gyp
     * @date 2023-07-24 18:49:44
     */
     void export(CarBrandParam carBrandParam);

}
