package yh.platform.modular.carbrand.controller;

import org.springframework.web.bind.annotation.GetMapping;
import yh.platform.core.pojo.response.ResponseData;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.core.pojo.response.SuccessResponseData;
import yh.platform.modular.carbrand.entity.CarBrand;
import yh.platform.modular.carbrand.param.CarBrandParam;
import yh.platform.modular.carbrand.service.CarBrandService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 品牌表控制器
 *
 * @author gyp
 * @date 2023-07-24 18:49:44
 */
@RestController
public class CarBrandController {

    @Resource
    private CarBrandService carBrandService;

//    /**
//     * 查询品牌表
//     *
//     * @author gyp
//     * @date 2023-07-24 18:49:44
//     */
//    @PostMapping("/carBrand/page")
//    public ResponseData<PageResult<CarBrand>> page(CarBrandParam carBrandParam) {
//        return SuccessResponseData.success(carBrandService.page(carBrandParam));
//    }

//    /**
//     * 添加品牌表
//     *
//     * @author gyp
//     * @date 2023-07-24 18:49:44
//     */
//    @PostMapping("/carBrand/add")
//    public ResponseData add(@RequestBody @Validated(CarBrandParam.add.class) CarBrandParam carBrandParam) {
//        carBrandService.add(carBrandParam);
//		return SuccessResponseData.success();
//    }

//    /**
//     * 删除品牌表，可批量删除
//     *
//     * @author gyp
//     * @date 2023-07-24 18:49:44
//     */
//    @PostMapping("/carBrand/delete")
//    public ResponseData delete(@RequestBody @Validated(CarBrandParam.delete.class) List<CarBrandParam> carBrandParamList) {
//        carBrandService.delete(carBrandParamList);
//		return SuccessResponseData.success();
//    }

//    /**
//     * 编辑品牌表
//     *
//     * @author gyp
//     * @date 2023-07-24 18:49:44
//     */
//    @PostMapping("/carBrand/edit")
//    public ResponseData edit(@RequestBody @Validated(CarBrandParam.edit.class) CarBrandParam carBrandParam) {
//        carBrandService.edit(carBrandParam);
//		return SuccessResponseData.success();
//    }

//    /**
//     * 查看品牌表
//     *
//     * @author gyp
//     * @date 2023-07-24 18:49:44
//     */
//    @PostMapping("/carBrand/detail")
//    public ResponseData<CarBrand> detail(@RequestBody @Validated(CarBrandParam.detail.class) CarBrandParam carBrandParam) {
//
//        return SuccessResponseData.success(carBrandService.detail(carBrandParam));
//    }

    /**
     * 品牌表列表
     *
     * @author gyp
     * @date 2023-07-24 18:49:44
     */
    @GetMapping("/carBrand/list")
    public ResponseData<List<CarBrand>> list(CarBrandParam carBrandParam) {
        return SuccessResponseData.success(carBrandService.list(carBrandParam));
    }

//    /**
//     * 导出系统用户
//     *
//     * @author gyp
//     * @date 2023-07-24 18:49:44
//     */
//    @PostMapping("/carBrand/export")
//    public void export(CarBrandParam carBrandParam) {
//        carBrandService.export(carBrandParam);
//    }

}
