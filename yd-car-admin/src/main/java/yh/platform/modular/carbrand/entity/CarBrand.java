package yh.platform.modular.carbrand.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import cn.afterturn.easypoi.excel.annotation.Excel;
import yh.platform.core.pojo.base.entity.BaseEntity;

/**
 * 品牌表
 *
 * @author gyp
 * @date 2023-07-24 18:49:44
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("car_brand")
public class CarBrand extends BaseEntity {

    /**
     *
     */
    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    /**
     * 品牌名称
     */
    @Excel(name = "品牌名称")
    private String brandName;

    /**
     * 首字母
     */
    @Excel(name = "首字母")
    private String firstLetterCapitalization;

    /**
     * 品牌编号
     */
    @Excel(name = "品牌编号")
    private String brandCode;

    /**
     * 品牌图片
     */
    @Excel(name = "品牌图片")
    private String brandUrl;

    /**
     * 0,否
            1,是
     */
    @Excel(name = "0,否  1,是")
    private Integer isHot;

    /**
     * 0,不展示
            1,展示
     */
    @Excel(name = "0,不展示 1,展示")
    private Integer isShow;

    /**
     * 0,否
            1,是
     */
    @Excel(name = "0,否 1,是")
    private Integer isParallelImport;

    /**
     * 热门车源排序
     */
    @Excel(name = "热门车源排序")
    private Integer sortNo;

    /**
     * 0,未删除
            1,已删除
     */
    @Excel(name = "0,未删除 1,已删除")
    private Integer deleteFlag;

    /**
     *
     */
    @Excel(name = "")
    private String remark;

}
