package yh.platform.modular.carbrand.param;

import yh.platform.core.pojo.base.param.BaseParam;
import lombok.Data;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.NotBlank;

/**
* 品牌表参数类
 *
 * @author gyp
 * @date 2023-07-24 18:49:44
*/
@Data
public class CarBrandParam extends BaseParam {

    /**
     *
     */
    @NotNull(message = "不能为空，请检查id参数", groups = {edit.class, delete.class, detail.class})
    private Long id;

    /**
     * 品牌名称
     */
    @NotBlank(message = "品牌名称不能为空，请检查brandName参数", groups = {add.class, edit.class})
    private String brandName;

    /**
     * 首字母
     */
    @NotBlank(message = "首字母不能为空，请检查firstLetterCapitalization参数", groups = {add.class, edit.class})
    private String firstLetterCapitalization;

    /**
     * 品牌编号
     */
    @NotBlank(message = "品牌编号不能为空，请检查brandCode参数", groups = {add.class, edit.class})
    private String brandCode;

    /**
     * 品牌图片
     */
    @NotBlank(message = "品牌图片不能为空，请检查brandUrl参数", groups = {add.class, edit.class})
    private String brandUrl;

    /**
     * 0,否
            1,是
     */
    @NotNull(message = "0,否1,是不能为空，请检查isHot参数", groups = {add.class, edit.class})
    private Integer isHot;

    /**
     * 0,不展示
            1,展示
     */
    @NotNull(message = "0,不展示1,展示不能为空，请检查isShow参数", groups = {add.class, edit.class})
    private Integer isShow;

    /**
     * 0,否
            1,是
     */
    @NotNull(message = "0,否1,是不能为空，请检查isParallelImport参数", groups = {add.class, edit.class})
    private Integer isParallelImport;

    /**
     * 热门车源排序
     */
    @NotNull(message = "热门车源排序不能为空，请检查sortNo参数", groups = {add.class, edit.class})
    private Integer sortNo;

    /**
     * 0,未删除
            1,已删除
     */
    @NotNull(message = "0,未删除1,已删除不能为空，请检查deleteFlag参数", groups = {add.class, edit.class})
    private Integer deleteFlag;

    /**
     *
     */
    @NotNull(message = "不能为空，请检查createBy参数", groups = {add.class, edit.class})
    private Long createBy;

    /**
     *
     */
    @NotNull(message = "不能为空，请检查updateBy参数", groups = {add.class, edit.class})
    private Long updateBy;

    /**
     *
     */
    @NotBlank(message = "不能为空，请检查remark参数", groups = {add.class, edit.class})
    private String remark;

}
