package yh.platform.modular.carbrand.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import yh.platform.modular.carbrand.entity.CarBrand;

/**
 * 品牌表
 *
 * @author gyp
 * @date 2023-07-24 18:49:44
 */
public interface CarBrandMapper extends BaseMapper<CarBrand> {
}
