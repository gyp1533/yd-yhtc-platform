package yh.platform.modular.carbrand.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import yh.platform.core.exception.ServiceException;
import yh.platform.core.factory.PageFactory;
import yh.platform.core.pojo.page.PageResult;
import yh.platform.core.util.PoiUtil;
import yh.platform.modular.carbrand.entity.CarBrand;
import yh.platform.modular.carbrand.enums.CarBrandExceptionEnum;
import yh.platform.modular.carbrand.mapper.CarBrandMapper;
import yh.platform.modular.carbrand.param.CarBrandParam;
import yh.platform.modular.carbrand.service.CarBrandService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 品牌表service接口实现类
 *
 * @author gyp
 * @date 2023-07-24 18:49:44
 */
@Service
public class CarBrandServiceImpl extends ServiceImpl<CarBrandMapper, CarBrand> implements CarBrandService {

    @Override
    public PageResult<CarBrand> page(CarBrandParam carBrandParam) {
        QueryWrapper<CarBrand> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(carBrandParam)) {

            // 根据品牌名称 查询
            if (ObjectUtil.isNotEmpty(carBrandParam.getBrandName())) {
                queryWrapper.lambda().eq(CarBrand::getBrandName, carBrandParam.getBrandName());
            }
            // 根据首字母 查询
            if (ObjectUtil.isNotEmpty(carBrandParam.getFirstLetterCapitalization())) {
                queryWrapper.lambda().eq(CarBrand::getFirstLetterCapitalization, carBrandParam.getFirstLetterCapitalization());
            }
            // 根据品牌编号 查询
            if (ObjectUtil.isNotEmpty(carBrandParam.getBrandCode())) {
                queryWrapper.lambda().eq(CarBrand::getBrandCode, carBrandParam.getBrandCode());
            }
            // 根据品牌图片 查询
            if (ObjectUtil.isNotEmpty(carBrandParam.getBrandUrl())) {
                queryWrapper.lambda().eq(CarBrand::getBrandUrl, carBrandParam.getBrandUrl());
            }
            // 根据0,否1,是 查询
            if (ObjectUtil.isNotEmpty(carBrandParam.getIsHot())) {
                queryWrapper.lambda().eq(CarBrand::getIsHot, carBrandParam.getIsHot());
            }
            // 根据0,不展示1,展示 查询
            if (ObjectUtil.isNotEmpty(carBrandParam.getIsShow())) {
                queryWrapper.lambda().eq(CarBrand::getIsShow, carBrandParam.getIsShow());
            }
            // 根据0,否1,是 查询
            if (ObjectUtil.isNotEmpty(carBrandParam.getIsParallelImport())) {
                queryWrapper.lambda().eq(CarBrand::getIsParallelImport, carBrandParam.getIsParallelImport());
            }
            // 根据热门车源排序 查询
            if (ObjectUtil.isNotEmpty(carBrandParam.getSortNo())) {
                queryWrapper.lambda().eq(CarBrand::getSortNo, carBrandParam.getSortNo());
            }
            // 根据0,未删除1,已删除 查询
            if (ObjectUtil.isNotEmpty(carBrandParam.getDeleteFlag())) {
                queryWrapper.lambda().eq(CarBrand::getDeleteFlag, carBrandParam.getDeleteFlag());
            }
            // 根据 查询
            if (ObjectUtil.isNotEmpty(carBrandParam.getRemark())) {
                queryWrapper.lambda().eq(CarBrand::getRemark, carBrandParam.getRemark());
            }
        }
        return new PageResult<>(this.page(PageFactory.defaultPage(), queryWrapper));
    }

    @Override
    public List<CarBrand> list(CarBrandParam carBrandParam) {
        return this.list();
    }

    @Override
    public void add(CarBrandParam carBrandParam) {
        CarBrand carBrand = new CarBrand();
        BeanUtil.copyProperties(carBrandParam, carBrand);
        this.save(carBrand);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<CarBrandParam> carBrandParamList) {
        carBrandParamList.forEach(carBrandParam -> {
            this.removeById(carBrandParam.getId());
        });
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(CarBrandParam carBrandParam) {
        CarBrand carBrand = this.queryCarBrand(carBrandParam);
        BeanUtil.copyProperties(carBrandParam, carBrand);
        this.updateById(carBrand);
    }

    @Override
    public CarBrand detail(CarBrandParam carBrandParam) {
        return this.queryCarBrand(carBrandParam);
    }

    /**
     * 获取品牌表
     *
     * @author gyp
     * @date 2023-07-24 18:49:44
     */
    private CarBrand queryCarBrand(CarBrandParam carBrandParam) {
        CarBrand carBrand = this.getById(carBrandParam.getId());
        if (ObjectUtil.isNull(carBrand)) {
            throw new ServiceException(CarBrandExceptionEnum.NOT_EXIST);
        }
        return carBrand;
    }

    @Override
    public void export(CarBrandParam carBrandParam) {
        List<CarBrand> list = this.list(carBrandParam);
        PoiUtil.exportExcelWithStream("CarBrand.xls", CarBrand.class, list);
    }

}
