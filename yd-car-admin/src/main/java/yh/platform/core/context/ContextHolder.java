package yh.platform.core.context;

import cn.hutool.core.util.ObjectUtil;
import yh.platform.core.context.login.LoginContextHolder;
import yh.platform.core.pojo.login.SysLoginUser;

public class ContextHolder {
    /**
     * 获取用户唯一id
     */
    public Long getUserUniqueId() {
        try {
            SysLoginUser sysLoginUser = LoginContextHolder.me().getSysLoginUserWithoutException();
            if(ObjectUtil.isNotNull(sysLoginUser)) {
                return sysLoginUser.getId();
            } else {
                return -1L;
            }
        } catch (Exception e) {
            //如果获取不到就返回-1
            return -1L;
        }
    }

    /**
     * 获取登录用户信息
     */
    public SysLoginUser getUser() {
        try {
            SysLoginUser sysLoginUser = LoginContextHolder.me().getSysLoginUserWithoutException();
            if(ObjectUtil.isNotNull(sysLoginUser)) {
                return sysLoginUser;
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }
}
