package yh.platform.core.enums;


public enum FilePathEnum {
    /* 车辆车头照片 */
    CAR_HEAD(101, "dy_car/car_source/head"),

    /* 前排座椅 */
    CAR_FRONT_SEAT(102, "dy_car/car_source/frontSeat"),

    /* 其他图 */
    CAR_OTHER(103, "dy_car/car_source/other"),

    /* 总控台 */
    CAR_MASTER_CONTROL_PANE(104, "dy_car/car_source/masterControlPane"),

    /* 仪表盘 */
    CAR_DASH_BOARD(105, "dy_car/car_source/dashBoard"),

    /* 损失图1 */
    CAR_LOSS_1(106, "dy_car/car_source/loss1"),

    /* 损失图2 */
    CAR_LOSS_2(107, "dy_car/car_source/loss2"),

    /* 天窗 */
    CAR_SKY_LIGHT(108, "dy_car/car_source/skyLight"),

    /* 右前45度 */
    CAR_FRONT_RIGHT_45(109, "dy_car/car_source/frontRight45"),

    /* 右后45度 */
    CAR_BACK_RIGHT_45(110, "dy_car/car_source/backRight45"),

    /* 铭牌 */
    CAR_NAMEPLATE(111, "dy_car/car_source/nameplate");


    private Integer fileType;

    private String path;

    FilePathEnum(final Integer fileType, final String path) {
        this.fileType = fileType;
        this.path = path;
    }

    public static String getPath(Integer fileType) {
        if (fileType != null) {
            for (FilePathEnum filePathEnum : FilePathEnum.values()) {
                if (filePathEnum.fileType().equals(fileType)) {
                    return filePathEnum.path();
                }
            }
        }
        return null;
    }

    public Integer fileType() {return fileType;}

    public String path() {
        return path;
    }
}
