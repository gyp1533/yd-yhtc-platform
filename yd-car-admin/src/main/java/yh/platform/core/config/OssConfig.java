package yh.platform.core.config;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSEncryptionClient;
import com.aliyun.oss.OSSEncryptionClientBuilder;
import com.aliyun.oss.crypto.SimpleRSAEncryptionMaterials;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.HashMap;
import java.util.Map;


@Component
public class OssConfig {

    /**
     * 域名
     */
    @Value("${oss.endpoint}")
    private String endpoint;

    /**
     * 账号
     */
    @Value("${oss.access_key_id}")
    public String accessKeyId;

    /**
     * 密匙
     */
    @Value("${oss.access_key_secret}")
    public String accessKeySecret;

    private static final String PRIVATE_PKCS8_PEM =
            "MIICeQIBADANBgkqhkiG9w0BAQEFAASCAmMwggJfAgEAAoGBANaxPhG3oZBngrXJ\n" +
                    "na0mbkMSWeuDgPDoRf+nq893x1JfDELk/Pj+OsMOmtqM+H9ZaXW2ZQZ1zwv07CdG\n" +
                    "pPzYmWWen6EFI+4DoLt4EVPnqhgOTpR2YhMu7z8t7SIoFF45Sb65QwAFx+UfxXI3\n" +
                    "UZZwCF6IBp7F0kc2YOnQxxsYuTkfAgMBAAECgYEAz7VI9MExOVE7v36g4v00XgWq\n" +
                    "I1Xl8Kz31AC3E5CxsXqtlSaPrKHh3gcKQHl0jVjG1BCzqs1dlBbQRqttQgaYWFex\n" +
                    "T6xeIPNNNNrt7mkFE4rqRGnXSWJnEELt9Aeu0/HCKAHSekMxm+z4NGrA4EJ7mdd9\n" +
                    "hy0pDwGPYpD1Xfid65ECQQDuAe2rYzyKigOGRw9uMPectrJaOm6OBDIOMQKV8N+5\n" +
                    "+4jZVLpBEIPsGIx+Uvvt2mawHb9OrbxLuKwGnIjsN+C5AkEA5uwaoc+5RZPEuxcm\n" +
                    "HMgN5CrP2QcqCsrtCq6pjyW8iSP6kDtbWbKp0pvCr2A+RQDdDtnYt0Ud55llE90c\n" +
                    "c5MMlwJBAIOGHUiwtiv4yKnTavuBo9O+QiEKWT2xo9ejzcH+MiQclCtFXwfFkKFP\n" +
                    "j5PfV8jBAC4iwi1AZOWTT7z3k9K1rkECQQCZKGGljst4DUD/QaRClKE6AvcmH61P\n" +
                    "yTWczymgysuYPeey/9tfy7L8/9gJT5EdXSxA7FXjgBEMid3TZeS39wDzAkEAxuct\n" +
                    "mqEKLXNj0wpWdLCV/J9OEzp8RyrplYYX5AgFk+zWHJD4HwQsrQNaIj8qK2blNF5c\n" +
                    "VIMFjMCxr0YbiUJQ/g==";

    // 填写您的RSA公钥字符串，可以使用OpenSSL工具生成。以下为RSA公钥字符串的示例值。
    private static final String PUBLIC_X509_PEM =
            "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDWsT4Rt6GQZ4K1yZ2tJm5DElnr\n" +
                    "g4Dw6EX/p6vPd8dSXwxC5Pz4/jrDDprajPh/WWl1tmUGdc8L9OwnRqT82Jllnp+h\n" +
                    "BSPuA6C7eBFT56oYDk6UdmITLu8/Le0iKBReOUm+uUMABcflH8VyN1GWcAheiAae\n" +
                    "xdJHNmDp0McbGLk5HwIDAQAB";

    @Bean
    public OSS ossClient() {
        return new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
    }

    @Bean
    public OSSEncryptionClient ossEncryptionClient() {
        RSAPrivateKey privateKey = SimpleRSAEncryptionMaterials.getPrivateKeyFromPemPKCS8(PRIVATE_PKCS8_PEM);
        RSAPublicKey publicKey = SimpleRSAEncryptionMaterials.getPublicKeyFromPemX509(PUBLIC_X509_PEM);
        KeyPair keyPair = new KeyPair(publicKey, privateKey);
        Map<String, String> matDesc = new HashMap<>();
        matDesc.put("oss-rsa-key1", "oss-rsa-value1");
        SimpleRSAEncryptionMaterials encryptionMaterials = new SimpleRSAEncryptionMaterials(keyPair, matDesc);
        return new OSSEncryptionClientBuilder().
                build(endpoint, accessKeyId, accessKeySecret, encryptionMaterials);
    }

}
