package yh.platform;

import cn.hutool.log.Log;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * SpringBoot方式启动类,主程序启动
 */
@SpringBootApplication
public class PlatformApplication {

    private static final Log log = Log.get();

    public static void main(String[] args) {
        SpringApplication.run(PlatformApplication.class, args);
        log.info("^_^ >>> " + PlatformApplication.class.getSimpleName() + " is success!");
    }

}
